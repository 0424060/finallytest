//
//  r_letterViewController.swift
//  0424038_HW0414
//
//  Created by ChenDing on 2017/5/19.
//  Copyright © 2017年 nkfust. All rights reserved.
//

import UIKit

class r_letterViewController: UIViewController {
    
    @IBOutlet var r_letterImageView:UIImageView!
    
    @IBOutlet var r_lettersChinese:UILabel!
    
    @IBOutlet var r_lettersEnglish:UILabel!
    
 var letters:R_letter!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        r_letterImageView.image = UIImage(named: letters.image)
        self.r_lettersChinese.text = letters.Chinesename
        self.r_lettersEnglish.text = letters.name
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
