//
//  R_letter.swift
//  0424038_HW0414
//
//  Created by ChenDing on 2017/5/26.
//  Copyright © 2017年 nkfust. All rights reserved.
//

import Foundation

class R_letter {
    var name = ""
    var Chinesename = ""
    var Phonetic = ""
    var image = ""
    
    init(name: String, Chinesename: String, Phonetic: String, image: String) {
        self.name = name
        self.Chinesename = Chinesename
        self.Phonetic = Phonetic
        self.image = image
    
    }
}


