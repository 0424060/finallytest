//
//  r letterTableViewController.swift
//  0424038_HW0414
//
//  Created by ChenDing on 2017/4/15.
//  Copyright © 2017年 nkfust. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class r_letterTableViewController: UITableViewController{
    var mycontext : NSManagedObjectContext!
    var letter: [LetterMO] = []
    
    var searchController:UISearchController!
    var searchResults:[LetterMO] = []
    
    let searchResultsController = UITableViewController()
    
    func getContext () -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    /*
    func filterContent(for searchText: String){
        searchResults = letters.filter({(R_letter)-> Bool in
            if let name = letters.name{
                let isMatch = name.localizedCaseInsensittiveContains(searchText)
                return isMatch
            }
            return false
        })
    }
    */
    
    //存入core data
        
    func saveEnglish(letters : [R_letter]){
        let context = getContext()
        for item in letters{
        var letter:LetterMO!
        letter = LetterMO(context: getContext())
        letter.english = item.name
        letter.chinese = item.Chinesename
        letter.phonetic = item.Phonetic
            if let letterImage = UIImage(named:item.image){
                if let imagedata = UIImageJPEGRepresentation (letterImage,1.0){
                    letter.image = NSData(data: imagedata)
                }
               letterss.append(letter)
            }
        }
        do {
            try context.save()
            print("success")
        } catch {
            print(error)
        }
    }
 
    
    //讀取 core data
    func getword() {
    let request:NSFetchRequest<LetterMO> = LetterMO.fetchRequest()
        let context = getContext()
        do{
            letter = try context.fetch(request)
            print(letter.count)
            for word in letter{
              print(word.chinese!)
              print(word.english!)
              print(word.phonetic!)
            }
        }catch{
            print(error)
        }
    }
    
    //刪除資料 
    func delete(){
    let request:NSFetchRequest<LetterMO> = LetterMO.fetchRequest()
     let context = getContext()
       
        do{
            letter = try context.fetch(request)
            for word in letter{
                print(word.english!)
                context.delete(word)
            }
            try context.save()
         print("delect")
        }catch{
            print(error)
        }

    }
    
    
    var letterss: [LetterMO] = []
    var letters : [R_letter] = [
        R_letter(name:"abacus",Chinesename:"算盤",Phonetic:"ˋæbəkəs",image:"abacus.jpg"),
        R_letter(name:"abbey",Chinesename:"修道院",Phonetic:"ˋæbɪ",image:"abbey.jpg"),
        R_letter(name:"ache",Chinesename:"痛",Phonetic:"ek",image:"ache.jpg"),
        R_letter(name:"acid",Chinesename:"酸",Phonetic:"ˋæsɪd",image:"acid.jpg"),
        R_letter(name:"actor",Chinesename:"男演員",Phonetic:"ˋæktɚ",image:"actor.png"),
        R_letter(name:"actress",Chinesename:"女演員",Phonetic:"ˋæktrɪs",image:"actress.jpg"),
        R_letter(name:"aim",Chinesename:"瞄準",Phonetic:"em",image:"aim.png"),
        R_letter(name:"air",Chinesename:"空氣",Phonetic:"ɛr",image:"air.jpg"),
        R_letter(name:"airplane",Chinesename:"飛機",Phonetic:"ˋɛr͵plen",image:"airplane.jpg"),
        R_letter(name:"aisle",Chinesename:"走廊",Phonetic:"aɪl",image:"aisle.jpg"),
        R_letter(name:"album",Chinesename:"相簿",Phonetic:"ˋælbəm",image:"album.jpg"),
        R_letter(name:"albumen",Chinesename:"蛋白",Phonetic:"ælˋbjumən",image:"albumen.png"),
        R_letter(name:"alcohol",Chinesename:"酒精",Phonetic:"ˋælkə͵hɔl",image:"alcohol.jpg"),
        R_letter(name:"alcoholic",Chinesename:"酒鬼",Phonetic:"͵ælkəˋhɔlɪk",image:"alcoholic.jpg"),
        R_letter(name:"alien",Chinesename:"外星人",Phonetic:"ˋelɪən",image:"alien.jpg"),
        R_letter(name:"all",Chinesename:"全部",Phonetic:"ɔl",image:"all.jpg"),
        R_letter(name:"alley",Chinesename:"小巷",Phonetic:"ˋælɪ",image:"alley.jpg"),
        R_letter(name:"alveolus",Chinesename:"肺泡",Phonetic:"ælˋviələs",image:"alveolus.jpg"),
        R_letter(name:"amazon",Chinesename:"女戰士"	,Phonetic:"ˋæmə͵zɑn",image:"amazon.jpg"),
        R_letter(name:"amethyst",Chinesename:"紫水晶",Phonetic:"ˋæməθɪst",image:"amethyst.jpg"),
        R_letter(name:"apple",Chinesename:"蘋果",Phonetic:"ˋæp!",image:"apple.jpg"),
        R_letter(name:"approval",Chinesename:"贊同",Phonetic:"əˋpruv!",image:"approval.jpg"),
        R_letter(name:"argument",Chinesename:"爭執",Phonetic:"ˋɑrgjəmənt",image:"argument.jpg"),
        R_letter(name:"arms",Chinesename:"武器",Phonetic:"ɑrmz",image:"arms.jpg"),
        R_letter(name:"ash",Chinesename:"灰燼",Phonetic:"æʃ",image:"ash.jpg"),
        R_letter(name:"assassin",Chinesename:"暗殺者",Phonetic:"əˋsæsɪn",image:"assassin.jpg"),
        R_letter(name:"attack",Chinesename:"攻擊",Phonetic:"əˋtæk",image:"attack.png"),
        R_letter(name:"awning",Chinesename:"遮雨棚",Phonetic:"ˋɔnɪŋ",image:"awning.jpg"),
        R_letter(name:"axe",Chinesename:"斧頭",Phonetic:"æks",image:"axe.jpg"),
        R_letter(name:"azalea",Chinesename:"杜鵑花",Phonetic:"əˋzeljə",image:"azalea.jpg"),
        R_letter(name:"Backward",Chinesename:"向後",Phonetic:"ˈbakwərd",image:"backward.jpg"),
        R_letter(name:"Bacteria",Chinesename:"病菌",Phonetic:"ˌbakˈtirēəm",image:"bacteria.jpg"),
        R_letter(name:"Badge",Chinesename:"徽章",Phonetic:"baj",image:"Badge.jpg"),
        R_letter(name:"Badminton",Chinesename:"羽毛球",Phonetic:"badmin(t)n",image:"badminton.jpg"),
        R_letter(name:"Baggage",Chinesename:"行李",Phonetic:"ˈbaɡij",image:"baggage.jpg"),
        R_letter(name:"Bait",Chinesename:"誘餌",Phonetic:"bāt",image:"Bait.jpg"),
        R_letter(name:"Bamboo",Chinesename:"竹子",Phonetic:"bamˈbo͞o",image:"Bamboo.jpg"),
        R_letter(name:"Barbarian",Chinesename:"野蠻人",Phonetic:"ˌbärˈberēən",image:"barbarian.jpg"),
        R_letter(name:"Barbecue",Chinesename:"烤肉",Phonetic:"ˈbärbəˌkyo͞o",image:"barbecue.jpg"),
        R_letter(name:"Barber",Chinesename:"理髮師",Phonetic:"ˈbärbər",image:"Barber.jpg"),
        R_letter(name:"Bartender",Chinesename:"調酒師",Phonetic:"bärˌtendər",image:"bartender.jpg"),
        R_letter(name:"Basement",Chinesename:"地下室",Phonetic:"ˈbāsmənt",image:"basement.jpg"),
        R_letter(name:"Basketball",Chinesename:"籃球",Phonetic:"ˋbæskɪt͵bɔl",image:"basketball.jpg"),
        R_letter(name:"Beautiful",Chinesename:"漂亮的",Phonetic:"ˈbyo͞odəfəl",image:"beautiful.jpg"),
        R_letter(name:"Bedroom",Chinesename:"臥室",Phonetic:"ˈbedˌro͞om",image:"bedroom.jpg"),
        R_letter(name:"Beetle",Chinesename:"甲蟲",Phonetic:"ˈbēdl",image:"Beetle.jpg"),
        R_letter(name:"Beverage",Chinesename:"飲料",Phonetic:"ˈbev(ə)rij",image:"Beverage.jpg"),
        R_letter(name:"Binoculars",Chinesename:"雙筒望遠鏡",Phonetic:"bəˈnäkyələrz",image:"Binoculars.jpg"),
        R_letter(name:"Biochemistry",Chinesename:"化學",Phonetic:"ˌbīōˈkeməstrē",image:"biochemistry.jpg"),
        R_letter(name:"Birthday",Chinesename:"生日",Phonetic:"ˈbərTHˌdā",image:"birthday.jpg"),
        R_letter(name:"Blaze",Chinesename:"火焰",Phonetic:"blāz",image:"Blaze.jpg"),
        R_letter(name:"Bodyguard",Chinesename:"保鏢",Phonetic:"bädēˌɡärd",image:"bodyguard.jpg"),
        R_letter(name:"Boxer",Chinesename:"拳擊手",Phonetic:"ˈbäksər",image:"Boxer.jpg"),
        R_letter(name:"Braid",Chinesename:"辮子",Phonetic:"brād",image:"Braid.jpg"),
        R_letter(name:"Bride",Chinesename:"新娘",Phonetic:"brīd",image:"Bride.jpg"),
        R_letter(name:"Bulb",Chinesename:"燈泡",Phonetic:"bəlb",image:"Bulb.jpg"),
        R_letter(name:"Burglar",Chinesename:"闖空門的人",Phonetic:"ˈbərɡlər",image:"Burglar.jpg"),
        R_letter(name:"Burial",Chinesename:"墳墓",Phonetic:"ˈberēəl",image:"Burial.jpg"),
        R_letter(name:"Butcher",Chinesename:"肉販",Phonetic:"ˈbo͝oCHər",image:"Butcher.jpg"),
        R_letter(name:"Butterfly",Chinesename:"蝴蝶",Phonetic:"ˋbʌtɚ͵flaɪ",image:"butterfly.jpg"),
        R_letter(name:"cabbage",Chinesename:"大白菜",Phonetic:"ˋkæbɪdʒ",image:"cabbage.jpg"),
        R_letter(name:"cabin",Chinesename:"船艙",Phonetic:"ˋkæbɪn",image:"cabin.jpg"),
        R_letter(name:"cactus",Chinesename:"仙人掌",Phonetic:"ˋkæktəs",image:"cactus.jpg"),
        R_letter(name:"cadaver",Chinesename:"屍體",Phonetic:"kəˋdævɚ",image:"cadaver.jpg"),
        R_letter(name:"cake",Chinesename:"蛋糕",Phonetic:"kek",image:"cake.jpg"),
        R_letter(name:"camp",Chinesename:"營地",Phonetic:"kæmp",image:"camp.jpg"),
        R_letter(name:"can",Chinesename:"罐頭",Phonetic:"kæn",image:"can.jpg"),
        R_letter(name:"candle",Chinesename:"蠟燭",Phonetic:"ˋkænd!",image:"candle.jpg"),
        R_letter(name:"candy",Chinesename:"糖果",Phonetic:"ˋkændɪ",image:"candy.jpg"),
        R_letter(name:"cane",Chinesename:"手杖",Phonetic:"ken",image:"cane.jpg"),
        R_letter(name:"canyon",Chinesename:"峽谷",Phonetic:"ˋkænjən",image:"canyon.jpg"),
        R_letter(name:"cap",Chinesename:"帽子",Phonetic:"kæp",image:"cap.jpg"),
        R_letter(name:"cape",Chinesename:"斗篷",Phonetic:"kep",image:"cape.jpg"),
        R_letter(name:"carrot",Chinesename:"胡蘿蔔",Phonetic:"ˋkærət",image:"carrot.jpg"),
        R_letter(name:"case",Chinesename:"盒子",Phonetic:"kes",image:"case.jpg"),
        R_letter(name:"cash",Chinesename:"現金",Phonetic:"kæʃ",image:"cash.jpg"),
        R_letter(name:"cask",Chinesename:"木桶",Phonetic:"kæsk",image:"cask.jpg"),
        R_letter(name:"cat",Chinesename:"貓",Phonetic:"kæt",image:"cat.jpg"),
        R_letter(name:"cattle",Chinesename:"牛",Phonetic:"ˋkæt!",image:"cattle.jpg"),
        R_letter(name:"cavalryman",Chinesename:"騎兵",Phonetic:"ˋkæv!rɪmən",image:"cavalryman.jpg"),
        R_letter(name:"cave",Chinesename:"山洞",Phonetic:"kev",image:"cave.jpg"),
        R_letter(name:"cayenne",Chinesename:"辣椒",Phonetic:"ˋkeɛn",image:"cayenne.jpg"),
        R_letter(name:"censer",Chinesename:"香爐",Phonetic:"ˋsɛnsɚ",image:"censer.jpg"),
        R_letter(name:"centipede",Chinesename:"蜈蚣",Phonetic:"ˋsɛntə͵pid",image:"centipede.jpg"),
        R_letter(name:"cerebrum",Chinesename:"大腦",Phonetic:"ˋsɛrəbrəm",image:"cerebrum.jpg"),
        R_letter(name:"champion",Chinesename:"冠軍",Phonetic:"ˋtʃæmpɪən",image:"champion.png"),
        R_letter(name:"charmer",Chinesename:"巫師",Phonetic:"ˋtʃɑrmɚ",image:"charmer.jpg"),
        R_letter(name:"chattel",Chinesename:"奴隸",Phonetic:"ˋtʃæt!",image:"chattel.jpg"),
        R_letter(name:"check",Chinesename:"支票",Phonetic:"tʃɛk",image:"check.jpg"),
        R_letter(name:"clothes",Chinesename:"衣服",Phonetic:"kloz",image:"clothes.jpg"),
        R_letter(name:"Dad",Chinesename:"爸爸",Phonetic:"dad",image:"Dad.jpg"),
        R_letter(name:"Dance",Chinesename:"跳舞",Phonetic:"dans",image:"Dance.jpg"),
        R_letter(name:"Danger",Chinesename:"危險",Phonetic:"ˈdānjər",image:"Danger.jpg"),
        R_letter(name:"Dark",Chinesename:"黑暗的",Phonetic:"därk",image:"Dark.jpg"),
        R_letter(name:"Daughter",Chinesename:"女兒",Phonetic:"ˈdôdər",image:"Daughter.jpg"),
        R_letter(name:"Day",Chinesename:"一天",Phonetic:"de",image:"Day.jpg"),
        R_letter(name:"Dead",Chinesename:"死的",Phonetic:"ded",image:"Dead.jpg"),
        R_letter(name:"Decline",Chinesename:"婉拒",Phonetic:"dəˈklīn",image:"Decline.jpg"),
        R_letter(name:"Decompose",Chinesename:"分解",Phonetic:"ˌdēkəmˈpōz",image:"Decompose.jpg"),
        R_letter(name:"Decrease",Chinesename:"減少",Phonetic:"dəˈkrēs",image:"Decrease.jpg"),
        R_letter(name:"Deep",Chinesename:"深的",Phonetic:"dēp",image:"Deep.jpg"),
        R_letter(name:"Deer",Chinesename:"鹿",Phonetic:"dir",image:"Deer.jpg"),
        R_letter(name:"Delete",Chinesename:"刪除",Phonetic:"dəˈlēt",image:"Delete.jpg"),
        R_letter(name:"Deliver",Chinesename:"傳送",Phonetic:"dəˈlivər",image:"Deliver.jpg"),
        R_letter(name:"Demand",Chinesename:"要求",Phonetic:"dəˈmand",image:"Demand.jpg"),
        R_letter(name:"Design",Chinesename:"設計",Phonetic:"dəˈzīn",image:"Design.jpg"),
        R_letter(name:"Diary",Chinesename:"日記",Phonetic:"ˈdī(ə)rē",image:"Diary.jpg"),
        R_letter(name:"Different",Chinesename:"不同的",Phonetic:"ˈdif(ə)rənt",image:"Different.jpg"),
        R_letter(name:"Director",Chinesename:"導演",Phonetic:"diˈrektər",image:"Director.jpg"),
        R_letter(name:"Discuss",Chinesename:"討論",Phonetic:"dəˈskəs",image:"Discuss.jpg"),
        R_letter(name:"Dish",Chinesename:"盤子",Phonetic:"diSH",image:"Dish.jpg"),
        R_letter(name:"Doctor",Chinesename:"醫生",Phonetic:"ˈdäktər",image:"Doctor.jpg"),
        R_letter(name:"Dog",Chinesename:"狗",Phonetic:"dôɡ",image:"Dog.jpg"),
        R_letter(name:"Double",Chinesename:"兩倍的",Phonetic:"ˈdəb(ə)l",image:"Double.jpg"),
        R_letter(name:"Doubt",Chinesename:"懷疑",Phonetic:"dout",image:"Doubt.jpg"),
        R_letter(name:"Down",Chinesename:"下降",Phonetic:"doun",image:"Down.jpg"),
        R_letter(name:"Draw",Chinesename:"畫",Phonetic:"drô",image:"Draw.jpg"),
        R_letter(name:"Dream",Chinesename:"夢想",Phonetic:"drēm",image:"Dream.jpg"),
        R_letter(name:"Dress",Chinesename:"裙子",Phonetic:"dres",image:"Dress.jpg"),
        R_letter(name:"Dry",Chinesename:"乾的",Phonetic:"drī",image:"Dry.jpg"),
        R_letter(name:"earnings",Chinesename:"薪水",Phonetic:"ˋɝnɪŋz",image:"earnings"),
        R_letter(name:"earpiece",Chinesename:"電話聽筒",Phonetic:"ɪr͵pis",image:"earpiece"),
        R_letter(name:"electronic",Chinesename:"電子的",Phonetic:"ɪlɛkˋtrɑnɪk",image:"electronic"),
        R_letter(name:"earlobe",Chinesename:"耳垂",Phonetic:"ˋɪr͵lob",image:"earlobe"),
        R_letter(name:"eardrum",Chinesename:"耳膜",Phonetic:"ˋɪr͵drʌm",image:"eardrum"),
        R_letter(name:"elbow",Chinesename:"手肘",Phonetic:"ˋɛlbo",image:"elbow"),
        R_letter(name:"earphone",Chinesename:"耳機",Phonetic:"ˋɪr͵fon",image:"earphone"),
        R_letter(name:"earmuffs",Chinesename:"耳罩",Phonetic:"ˋɪr͵mʌf",image:"earmuffs"),
        R_letter(name:"eyelid",Chinesename:"眼皮",Phonetic:"ˋaɪ͵lɪd",image:"eyelid"),
        R_letter(name:"equalize",Chinesename:"均衡",Phonetic:"ˋikwəl͵aɪz",image:"equalize"),
        R_letter(name:"ear",Chinesename:"耳朵",Phonetic:"ir",image:"EAR"),
        R_letter(name:"earth",Chinesename:"地球",Phonetic:"ərTH",image:"EARTH"),
        R_letter(name:"eye",Chinesename:"眼睛",Phonetic:"ī",image:"EYE"),
        R_letter(name:"economy",Chinesename:"經濟",Phonetic:"əˈkänəmē",image:"economy"),
        R_letter(name:"envelope",Chinesename:"信封",Phonetic:"ənˈveləp",image:"envelope"),
        R_letter(name:"exciting",Chinesename:"興奮",Phonetic:"ikˈsīdiNG",image:"exciting"),
        R_letter(name:"nameineer",Chinesename:"工程師",Phonetic:"ˌenjəˈnir",image:"nameineer"),
        R_letter(name:"e-mail",Chinesename:"電子郵件",Phonetic:"ˈēmā",image:"e-mail"),
        R_letter(name:"eat",Chinesename:"吃",Phonetic:"ēt",image:"eat"),
        R_letter(name:"elementary",Chinesename:"小學",Phonetic:"eləˈment(ə)rē",image:"elementary"),
        R_letter(name:"eight",Chinesename:"八",Phonetic:"et",image:"eight"),
        R_letter(name:"eraser",Chinesename:"橡皮擦",Phonetic:"ɪˋresɚ",image:"eraser"),
        R_letter(name:"embassy",Chinesename:"大使館",Phonetic:"ˋɛmbəsɪ",image:"embassy"),
        R_letter(name:"encyclopedia",Chinesename:"百科全書",Phonetic:"ɪn͵saɪkləˋpidɪə",image:"encyclopedia"),
        R_letter(name:"emperor",Chinesename:"皇帝",Phonetic:"ˋɛmpərɚ",image:"emperor"),
        R_letter(name:"europe",Chinesename:"歐洲",Phonetic:"ˋjʊrəp",image:"europe"),
        R_letter(name:"eagle",Chinesename:"老鷹",Phonetic:"ˋig!",image:"eagle"),
        R_letter(name:"earplug",Chinesename:"耳塞",Phonetic:"ˋɪr͵plʌg",image:"earplug"),
        R_letter(name:"earthworm",Chinesename:"蚯蚓",Phonetic:"ˋɝθ͵wɝm",image:"earthworm"),
        R_letter(name:"eel",Chinesename:"鰻魚",Phonetic:"il",image:"eel"),
        R_letter(name:"face",Chinesename:"臉",Phonetic:"fes",image:"face.png"),
        R_letter(name:"factory",Chinesename:"工廠",Phonetic:"ˋfæməlɪ",image:"factory.jpg"),
        R_letter(name:"family",Chinesename:"家庭",Phonetic:"fæn",image:"family.png"),
        R_letter(name:"fan",Chinesename:"扇子",Phonetic:"fɑr",image:"fan.png"),
        R_letter(name:"far",Chinesename:"遠的",Phonetic:"fɑrm",image:"far.png"),
        R_letter(name:"farm",Chinesename:"農場",Phonetic:"ˋfɑrmɚ",image:"farm.png"),
        R_letter(name:"farmer",Chinesename:"農夫",Phonetic:"fæst",image:"farmer.png"),
        R_letter(name:"fast",Chinesename:"快的",Phonetic:"fæt",image:"fast.png"),
        R_letter(name:"fat",Chinesename:"胖的",Phonetic:"ˋfɑðɚ",image:"fat.png"),
        R_letter(name:"father",Chinesename:"父親",Phonetic:"ˋfæktərɪ",image:"father.png"),
        R_letter(name:"fifteen",Chinesename:"十五",Phonetic:"ˋfɪfˋtin",image:"fifteen.jpg"),
        R_letter(name:"fight",Chinesename:"戰鬥",Phonetic:"faɪt",image:"fight.jpg"),
        R_letter(name:"fireplace",Chinesename:"壁爐",Phonetic:"ˋfaɪr͵ples",image:"fireplace.jpg"),
        R_letter(name:"firework",Chinesename:"煙火",Phonetic:"ˋfaɪr͵wɝk",image:"firework.jpg"),
        R_letter(name:"flame",Chinesename:"火焰",Phonetic:"flem",image:"flame.jpg"),
        R_letter(name:"flea",Chinesename:"跳蚤",Phonetic:"fli",image:"flea.jpg"),
        R_letter(name:"flood",Chinesename:"水災",Phonetic:"flʌd",image:"flood.jpg"),
        R_letter(name:"flower",Chinesename:"花",Phonetic:"ˋflaʊɚ",image:"flower.jpg"),
        R_letter(name:"flute",Chinesename:"橫笛",Phonetic:"flut",image:"flute.jpg"),
        R_letter(name:"fly",Chinesename:"蒼蠅",Phonetic:"flaɪ",image:"fly.jpg"),
        R_letter(name:"fod",Chinesename:"食物",Phonetic:"fud",image:"food.jpg"),
        R_letter(name:"foot",Chinesename:"腳",Phonetic:"fʊt",image:"foot.jpg"),
        R_letter(name:"forge",Chinesename:"冶煉場",Phonetic:"fɔrdʒ",image:"forge.jpg"),
        R_letter(name:"fork",Chinesename:"叉子",Phonetic:"fɔrk",image:"fork.jpg"),
        R_letter(name:"fort",Chinesename:"堡壘",Phonetic:"fort",image:"fort.jpg"),
        R_letter(name:"fossil",Chinesename:"化石",Phonetic:"ˋfɑs",image:"fossil.jpg"),
        R_letter(name:"fox",Chinesename:"狐狸",Phonetic:"fɑks",image:"fox.jpg"),
        R_letter(name:"frog",Chinesename:"青蛙",Phonetic:"frɑg",image:"frog.jpg"),
        R_letter(name:"funeral",Chinesename:"葬禮",Phonetic:"ˋfjunərəl",image:"funeral.jpg"),
        R_letter(name:"funny",Chinesename:"好笑的",Phonetic:"ˋfʌnɪ",image:"funny.jpg"),
        R_letter(name:"gambler",Chinesename:"賭徒",Phonetic:"ˋgæmb!ɚ",image:"gambler.jpg"),
        R_letter(name:"game",Chinesename:"遊戲",Phonetic:"gem",image:"game.jpg"),
        R_letter(name:"gangster",Chinesename:"匪徒",Phonetic:"ˋgæŋstɚ",image:"gangster.jpg"),
        R_letter(name:"gaol",Chinesename:"監獄",Phonetic:"ˋdʒel",image:"gaol.jpg"),
        R_letter(name:"gap",Chinesename:"缺口",Phonetic:"gæp",image:"gap.jpg"),
        R_letter(name:"garbage",Chinesename:"垃圾",Phonetic:"ˋgɑrbɪdʒ",image:"garbage.jpg"),
        R_letter(name:"garden",Chinesename:"花園",Phonetic:"ˋgɑrdn",image:"garden.jpg"),
        R_letter(name:"gardener",Chinesename:"園丁",Phonetic:"ˋgɑrdənɚ",image:"gardener.jpg"),
        R_letter(name:"garlic",Chinesename:"大蒜",Phonetic:"ˋgɑrlɪk",image:"garlic.jpg"),
        R_letter(name:"garnet",Chinesename:"石榴石",Phonetic:"ˋgɑrnɪt",image:"garnet.jpg"),
        R_letter(name:"gasoline",Chinesename:"汽油",Phonetic:"ˋgæsə͵lin",image:"gasoline.jpg"),
        R_letter(name:"gate",Chinesename:"大門",Phonetic:"get",image:"gate.jpg"),
        R_letter(name:"gauze",Chinesename:"薄紗",Phonetic:"gɔz",image:"gauze.jpg"),
        R_letter(name:"gaze",Chinesename:"注視",Phonetic:"gez",image:"gaze.jpg"),
        R_letter(name:"gender",Chinesename:"性別",Phonetic:"ˋdʒɛndɚ",image:"gender.jpg"),
        R_letter(name:"germ",Chinesename:"細菌",Phonetic:"dʒɝm",image:"germ.jpg"),
        R_letter(name:"ghost",Chinesename:"鬼魂",Phonetic:"gost",image:"ghost.png"),
        R_letter(name:"giant",Chinesename:"巨人",Phonetic:"ˋdʒaɪənt",image:"giant.jpg"),
        R_letter(name:"gift",Chinesename:"禮物",Phonetic:"gɪft",image:"gift.jpg"),
        R_letter(name:"ginger",Chinesename:"生薑",Phonetic:"ˋdʒɪndʒɚ",image:"ginger.jpg"),
        R_letter(name:"girl",Chinesename:"女孩",Phonetic:"gɝl",image:"girl.jpg"),
        R_letter(name:"glass",Chinesename:"玻璃",Phonetic:"glæs",image:"glass.jpg"),
        R_letter(name:"glen",Chinesename:"峽谷",Phonetic:"glɛn",image:"glen.jpg"),
        R_letter(name:"global",Chinesename:"球狀的",Phonetic:"ˋglob!",image:"global.jpg"),
        R_letter(name:"glove",Chinesename:"手套",Phonetic:"glʌv",image:"glove.jpg"),
        R_letter(name:"goat",Chinesename:"山羊",Phonetic:"got",image:"goat.jpg"),
        R_letter(name:"god",Chinesename:"上帝",Phonetic:"gɑd",image:"god.jpg"),
        R_letter(name:"goods",Chinesename:"商品",Phonetic:"gʊdz",image:"goods.jpg"),
        R_letter(name:"goose",Chinesename:"鵝",Phonetic:"gus",image:"goose.jpg"),
        R_letter(name:"gorilla",Chinesename:"大猩猩",Phonetic:"gəˋrɪlə",image:"gorilla.jpg"),
        R_letter(name:"hack",Chinesename:"老馬",Phonetic:"hak",image:"hack.jpg"),
        R_letter(name:"hamburger",Chinesename:"漢堡",Phonetic:"ˈhamˌbərgər",image:"hamburger.jpg"),
        R_letter(name:"hammer",Chinesename:"榔頭",Phonetic:"hamər",image:"hammer.jpg"),
        R_letter(name:"hand",Chinesename:"手",Phonetic:"hand",image:"hand.jpg"),
        R_letter(name:"hand job",Chinesename:"打手槍",Phonetic:"handjäb",image:"hand job.jpg"),
        R_letter(name:"handle",Chinesename:"處理",Phonetic:"ˈhandl",image:"handle.jpg"),
        R_letter(name:"handsome",Chinesename:"英俊的",Phonetic:"ˈhansəm",image:"handsome.jpg"),
        R_letter(name:"hang",Chinesename:"把...掛起",Phonetic:"han",image:"hang.jpg"),
        R_letter(name:"happy",Chinesename:"快樂",Phonetic:"ˈhapē",image:"happy.jpg"),
        R_letter(name:"hard",Chinesename:"難",Phonetic:"hard",image:"hard.jpg"),
        R_letter(name:"hat",Chinesename:"帽子",Phonetic:"hat",image:"hat.jpg"),
        R_letter(name:"hate",Chinesename:"討厭",Phonetic:"hāt",image:"hate.jpg"),
        R_letter(name:"hen",Chinesename:"母雞",Phonetic:"hen]",image:"hen.jpg"),
        R_letter(name:"hide",Chinesename:"隱藏",Phonetic:"hīd",image:"hide.jpg"),
        R_letter(name:"highlight",Chinesename:"使顯著",Phonetic:"ˈhīlīt",image:"highlight.jpg"),
        R_letter(name:"hit",Chinesename:"打",Phonetic:"hit",image:"hit.jpg"),
        R_letter(name:"hold",Chinesename:"握著",Phonetic:"hold",image:"hold.jpg"),
        R_letter(name:"honor",Chinesename:"榮譽",Phonetic:"ˈänər",image:"honor.jpg"),
        R_letter(name:"hook",Chinesename:"掛鉤",Phonetic:"ho͝ok",image:"hook.jpg"),
        R_letter(name:"hooker",Chinesename:"妓女",Phonetic:"ho͝okər",image:"hooker.jpg"),
        R_letter(name:"hooligan",Chinesename:"小流氓",Phonetic:"ˈho͞oləgin",image:"hooligan.jpg"),
        R_letter(name:"hope",Chinesename:"希望",Phonetic:"hōp",image:"hope.jpg"),
        R_letter(name:"horse",Chinesename:"馬",Phonetic:"hôrs",image:"horse.jpg"),
        R_letter(name:"hospital",Chinesename:"醫院",Phonetic:"ˈhäˌspitl",image:"hospital.jpg"),
        R_letter(name:"host",Chinesename:"主人",Phonetic:"host",image:"host.jpg"),
        R_letter(name:"hot",Chinesename:"熱",Phonetic:"hät",image:"hot.jpg"),
        R_letter(name:"hotel",Chinesename:"旅館",Phonetic:"hōˈtel",image:"hotel.jpg"),
        R_letter(name:"house",Chinesename:"房子",Phonetic:"hous",image:"house.jpg"),
        R_letter(name:"humble",Chinesename:"謙虛",Phonetic:"ˈhəmbəl",image:"humble.jpg"),
        R_letter(name:"ice",Chinesename:"冰",Phonetic:"īs",image:"ice.jpg"),
        R_letter(name:"ice cream",Chinesename:"冰淇淋",Phonetic:"ˌaɪs ˈkrim",image:"ice cream.jpg"),
        R_letter(name:"icon",Chinesename:"圖標",Phonetic:"īˌkän",image:"icon.png"),
        R_letter(name:"idea",Chinesename:"想法",Phonetic:"īdz",image:"idea.jpg"),
        R_letter(name:"ideal",Chinesename:"理想;志向",Phonetic:"īˈdēəl",image:"ideal.png"),
        R_letter(name:"ident",Chinesename:"識別",Phonetic:"ˈīdent",image:"ident.jpg"),
        R_letter(name:"idol",Chinesename:"偶像",Phonetic:"ˈīdl",image:"idol.jpg"),
        R_letter(name:"illegal",Chinesename:"非法的",Phonetic:"ilˈlēɡəl",image:"illegal.jpg"),
        R_letter(name:"illness",Chinesename:"疾病",Phonetic:"ˈilnəs",image:"illness.jpg"),
        R_letter(name:"important",Chinesename:"重要的",Phonetic:"imˈpôrtnt",image:"important.jpg"),
        R_letter(name:"incident",Chinesename:"事件",Phonetic:"ˈinsədənt",image:"incident.png"),
        R_letter(name:"income",Chinesename:"收入",Phonetic:"ˈinˌkəm",image:"income.jpg"),
        R_letter(name:"increase",Chinesename:"增加",Phonetic:"inˈkrēs",image:"increase.jpg"),
        R_letter(name:"independent",Chinesename:"獨立的",Phonetic:"ˌindəˈpendənt",image:"independent.jpg"),
        R_letter(name:"indicate",Chinesename:"指示",Phonetic:"ˈindəˌkāt",image:"indicate.jpg"),
        R_letter(name:"industry",Chinesename:"工業;行業",Phonetic:"ˈindəstrē",image:"industry.jpg"),
        R_letter(name:"information",Chinesename:"資訊;訊息",Phonetic:"ˌinfərˈmāSH(ən",image:"information.jpg"),
        R_letter(name:"injure",Chinesename:"傷害",Phonetic:"ˈinjər",image:"injure.jpg"),
        R_letter(name:"insect",Chinesename:"昆蟲",Phonetic:"ˈinˌsekt",image:"insect.jpg"),
        R_letter(name:"intelligence",Chinesename:"智力",Phonetic:"inˈteləjəns",image:"intelligence.jpg"),
        R_letter(name:"international",Chinesename:"國際的",Phonetic:"ˌin(tərˈnaSH(ən(əl",image:"international.jpg"),
        R_letter(name:"internet",Chinesename:"網際網路",Phonetic:"ˈin(tərˌnet",image:"internet.jpg"),
        R_letter(name:"invade",Chinesename:"入侵",Phonetic:"inˈvād",image:"invade.jpg"),
        R_letter(name:"invent",Chinesename:"發明",Phonetic:"inˈvent",image:"invent.jpg"),
        R_letter(name:"invest",Chinesename:"投資",Phonetic:"inˈvest",image:"invest.jpg"),
        R_letter(name:"investigate",Chinesename:"調查",Phonetic:"inˈvestəˌɡāt",image:"investigate.jpg"),
        R_letter(name:"invite",Chinesename:"邀請",Phonetic:"inˈvīt",image:"invite.png"),
        R_letter(name:"iron",Chinesename:"鐵",Phonetic:"ī(ərn",image:"iron.jpg"),
        R_letter(name:"island",Chinesename:"島嶼",Phonetic:"ˈīlənd",image:"island.jpg"),
        R_letter(name:"issue",Chinesename:"議題",Phonetic:"ˈiSHo͞o",image:"issue.jpg"),
        R_letter(name:"jacal",Chinesename:"小茅屋",Phonetic:"hɑˋkɑl",image:"jacal.jpg"),
        R_letter(name:"jacana",Chinesename:"水雉",Phonetic:"͵ʒɑsəˋnɑ",image:"jacana.jpg"),
        R_letter(name:"jackal",Chinesename:"豺狼",Phonetic:"ˋdʒækɔl",image:"jackal.jpg"),
        R_letter(name:"jackboot",Chinesename:"長筒靴",Phonetic:"ˋdʒæk͵but",image:"jackboot.jpg"),
        R_letter(name:"jacket",Chinesename:"夾克",Phonetic:"ˋdʒækIt",image:"jacket.jpg"),
        R_letter(name:"jackfruit",Chinesename:"菠蘿蜜",Phonetic:"ˋdʒæk͵frut",image:"jackfruit.jpg"),
        R_letter(name:"jackknife",Chinesename:"摺疊刀",Phonetic:"ˋdʒæk͵naɪf",image:"jackknife.jpg"),
        R_letter(name:"jackshaft",Chinesename:"起重機",Phonetic:"ˋdʒæk͵ʃæft",image:"jackshaft.jpg"),
        R_letter(name:"jail",Chinesename:"監獄",Phonetic:"dʒel",image:"jail.jpg"),
        R_letter(name:"jam",Chinesename:"果醬",Phonetic:"dʒæm",image:"jam.jpg"),
        R_letter(name:"jamb",Chinesename:"門邊框柱",Phonetic:"dʒæm",image:"jamb.jpg"),
        R_letter(name:"jazz",Chinesename:"爵士樂",Phonetic:"dʒæz",image:"jazz.jpg"),
        R_letter(name:"jeans",Chinesename:"牛仔褲",Phonetic:"dʒinz",image:"jeans.jpg"),
        R_letter(name:"jelly",Chinesename:"果凍",Phonetic:"ˋdʒɛlɪ",image:"jelly.jpg"),
        R_letter(name:"jet",Chinesename:"噴射機",Phonetic:"dʒZt",image:"jet.jpg"),
        R_letter(name:"jewelry",Chinesename:"珠寶",Phonetic:"ˋdʒuәlrI",image:"jewelry.jpg"),
        R_letter(name:"job",Chinesename:"工作",Phonetic:"dʒɑb",image:"job.jpg"),
        R_letter(name:"jog",Chinesename:"慢跑",Phonetic:"dʒɑg",image:"jog.jpg"),
        R_letter(name:"joke",Chinesename:"笑話",Phonetic:"dʒok",image:"joke.jpg"),
        R_letter(name:"journal",Chinesename:"日誌",Phonetic:"ˋdʒFnL",image:"journal.jpg"),
        R_letter(name:"journey",Chinesename:"旅行",Phonetic:"ˋdʒFnI",image:"journey.jpg"),
        R_letter(name:"joy",Chinesename:"歡樂",Phonetic:"dʒɔɪ",image:"joy.jpg"),
        R_letter(name:"judge",Chinesename:"法官",Phonetic:"dʒʌdʒ",image:"judge.jpg"),
        R_letter(name:"jug",Chinesename:"水壺",Phonetic:"dʒʌg",image:"jug.jpg"),
        R_letter(name:"juice",Chinesename:"果汁",Phonetic:"dʒus",image:"juice.jpg"),
        R_letter(name:"jump",Chinesename:"跳",Phonetic:"dʒʌmp",image:"jump.jpg"),
        R_letter(name:"jungle",Chinesename:"叢林",Phonetic:"ˋdʒʌŋgL",image:"jungle.jpg"),
        R_letter(name:"junk",Chinesename:"垃圾",Phonetic:"dʒʌŋk",image:"junk.jpg"),
        R_letter(name:"jury",Chinesename:"陪審團",Phonetic:"ˈdʒʊrɪ",image:"jury.jpg"),
        R_letter(name:"justice",Chinesename:"公平",Phonetic:"ˋdʒʌstIs",image:"justice.jpg"),
        R_letter(name:"Kaiser",Chinesename:"皇帝",Phonetic:"ˋkaɪzɚ",image:"Kaiser.jpg"),
        R_letter(name:"kaleidoscope",Chinesename:"萬花筒",Phonetic:"kəˋlaɪdə͵skop",image:"kaleidoscope.jpg"),
        R_letter(name:"kangaroo",Chinesename:"袋鼠",Phonetic:"͵kæŋgəˋru",image:"kangaroo.jpg"),
        R_letter(name:"karate",Chinesename:"空手道",Phonetic:"kəˋrɑtɪ",image:"karate.jpg"),
        R_letter(name:"keel",Chinesename:"平底船",Phonetic:"kil",image:"keel.jpg"),
        R_letter(name:"kennel",Chinesename:"狗窩",Phonetic:"ˋkɛn!",image:"kennel.jpg"),
        R_letter(name:"kerchief",Chinesename:"手帕",Phonetic:"ˋkɝtʃɪf",image:"kerchief.jpg"),
        R_letter(name:"kettle",Chinesename:"水壺",Phonetic:"ˋkɛt!",image:"kettle.jpg"),
        R_letter(name:"key",Chinesename:"鑰匙",Phonetic:"ki",image:"key.png"),
        R_letter(name:"keyboard",Chinesename:"鍵盤",Phonetic:"ˋki͵bord",image:"keyboard.jpg"),
        R_letter(name:"keypad",Chinesename:"小鍵盤",Phonetic:"ˋki͵pæd",image:"keypad.jpg"),
        R_letter(name:"kid",Chinesename:"少年",Phonetic:"kɪd",image:"kid.jpg"),
        R_letter(name:"kill",Chinesename:"殺",Phonetic:"kɪl",image:"kill.jpg"),
        R_letter(name:"killer",Chinesename:"殺手",Phonetic:"ˋkɪlɚ",image:"killer.jpg"),
        R_letter(name:"kindergarten",Chinesename:"幼稚園",Phonetic:"ˋkɪndɚ͵gɑrtn",image:"kindergarten.jpg"),
        R_letter(name:"king",Chinesename:"國王",Phonetic:"kɪŋ",image:"king.png"),
        R_letter(name:"kip",Chinesename:"旅店",Phonetic:"kɪp",image:"kip.jpg"),
        R_letter(name:"kiss",Chinesename:"吻",Phonetic:"kɪs",image:"kiss.jpg"),
        R_letter(name:"kitchen",Chinesename:"廚房",Phonetic:"ˋkɪtʃɪn",image:"kitchen.jpg"),
        R_letter(name:"kite",Chinesename:"風箏",Phonetic:"kaɪt",image:"kite.jpg"),
        R_letter(name:"kitten",Chinesename:"小貓",Phonetic:"ˋkɪtn",image:"kitten.jpg"),
        R_letter(name:"knead",Chinesename:"揉",Phonetic:"nid",image:"knead.jpg"),
        R_letter(name:"knee",Chinesename:"膝蓋",Phonetic:"ni",image:"knee.jpg"),
        R_letter(name:"kneel",Chinesename:"跪下",Phonetic:"nil",image:"kneel.png"),
        R_letter(name:"knife",Chinesename:"刀子",Phonetic:"naɪf",image:"knife.jpg"),
        R_letter(name:"knight",Chinesename:"騎士",Phonetic:"naɪt",image:"knight.jpg"),
        R_letter(name:"knob",Chinesename:"門把",Phonetic:"nɑb",image:"knob.jpg"),
        R_letter(name:"knock",Chinesename:"敲",Phonetic:"nɑk",image:"knock.jpg"),
        R_letter(name:"knot",Chinesename:"繩結",Phonetic:"nɑt",image:"knot.jpg"),
        R_letter(name:"ksyak",Chinesename:"獨木舟",Phonetic:"ˈkaɪæk",image:"ksyak.jpg"),
        R_letter(name:"lace",Chinesename:"緞帶",Phonetic:"lās",image:"lacep.jpg"),
        R_letter(name:"lack",Chinesename:"缺乏",Phonetic:"lak",image:"lackp.jpg"),
        R_letter(name:"lackadaisical",Chinesename:"不客氣",Phonetic:"ˌlakəˈdāzək(ə)l",image:"lackadaisicalp.png"),
        R_letter(name:"lacrimal",Chinesename:"眼淚",Phonetic:"ˈlakrəməl",image:"lacrimalp.png"),
        R_letter(name:"lactic",Chinesename:"乳酸",Phonetic:"ˈlaktik",image:"lacticp.jpg"),
        R_letter(name:"lacuna",Chinesename:"空白",Phonetic:"ləˈk(y)o͞onə",image:"lacunap.png"),
        R_letter(name:"laden",Chinesename:"負擔",Phonetic:"lādn",image:"ladenp.png"),
        R_letter(name:"lady",Chinesename:"女士",Phonetic:"ˈlādē",image:"ladyp.jpg"),
        R_letter(name:"laic",Chinesename:"凡人",Phonetic:"ˈlāik",image:"laicp.png"),
        R_letter(name:"lake",Chinesename:"湖",Phonetic:"lāk",image:"lakep.jpg"),
        R_letter(name:"lamb",Chinesename:"羔羊",Phonetic:"lam",image:"lambp.jpg"),
        R_letter(name:"lamp",Chinesename:"檯燈",Phonetic:"lamp",image:"lampp.jpg"),
        R_letter(name:"land",Chinesename:"陸地",Phonetic:"land",image:"landp.jpg"),
        R_letter(name:"language",Chinesename:"語言",Phonetic:"ˈlaNGɡwij",image:"languagep.png"),
        R_letter(name:"laugh",Chinesename:"笑",Phonetic:"laf",image:"laughp.png"),
        R_letter(name:"lawyer",Chinesename:"律師",Phonetic:"ˈloiər",image:"lawyerp.jpg"),
        R_letter(name:"lazy",Chinesename:"懶惰",Phonetic:"ˈlāzē",image:"lazyp.png"),
        R_letter(name:"leave",Chinesename:"離開",Phonetic:"lēv",image:"leavep.jpg"),
        R_letter(name:"leg",Chinesename:"腳",Phonetic:"leɡ",image:"legp.png"),
        R_letter(name:"leR_lettern",Chinesename:"檸檬",Phonetic:"ˈlemən",image:"leR_letternp.jpg"),
        R_letter(name:"lesson",Chinesename:"課堂",Phonetic:"ˈles(ə)n",image:"lessonp.png"),
        R_letter(name:"lettuce",Chinesename:"萵苣",Phonetic:"ˈledəs",image:"lettucep.jpg"),
        R_letter(name:"lid",Chinesename:"蓋子",Phonetic:"lid",image:"lidp.jpg"),
        R_letter(name:"lie",Chinesename:"謊言",Phonetic:"lī",image:"liep.jpg"),
        R_letter(name:"life",Chinesename:"生命",Phonetic:"līf",image:"lifep.jpg"),
        R_letter(name:"limit",Chinesename:"極限",Phonetic:"ˈlimit",image:"limitp.png"),
        R_letter(name:"location",Chinesename:"位置",Phonetic:"lōˈkāSH(ə)n",image:"locationp.png"),
        R_letter(name:"lolly",Chinesename:"棒棒糖",Phonetic:"ˈlälē",image:"lollyp.png"),
        R_letter(name:"lose",Chinesename:"喪失",Phonetic:"lo͞oz",image:"losep.jpg"),
        R_letter(name:"lucky",Chinesename:"幸運",Phonetic:"ˈləkē",image:"luckyp.jpg"),
        R_letter(name:"macadam",Chinesename:"碎石路",Phonetic:"məˋkædəm",image:"macadam.jpg"),
        R_letter(name:"macaron",Chinesename:"蛋白杏仁餅乾",Phonetic:"͵mækəˋrun",image:"macaroon.jpg"),
        R_letter(name:"macaroni",Chinesename:"通心麵",Phonetic:"͵mækəˋronɪ",image:"macaroni.jpg"),
        R_letter(name:"mace",Chinesename:"權杖",Phonetic:"mes",image:"mace.jpg"),
        R_letter(name:"machete",Chinesename:"大砍刀",Phonetic:"mɑˋtʃete",image:"machete.jpg"),
        R_letter(name:"mackerel",Chinesename:"鯖魚",Phonetic:"ˋmækərəl",image:"mackerel.jpg"),
        R_letter(name:"mackintosh",Chinesename:"風衣",Phonetic:"ˈmækɪntɒʃ",image:"mackintosh.jpg"),
        R_letter(name:"Madonna",Chinesename:"聖母瑪利亞",Phonetic:"məˋdɑnə",image:"Madonna.jpg"),
        R_letter(name:"maelstorm",Chinesename:"混亂",Phonetic:"ˈmeɪlstrəm",image:"maelstorm.jpg"),
        R_letter(name:"magazine",Chinesename:"雜誌",Phonetic:"͵mægəˋzin",image:"magazine.jpg"),
        R_letter(name:"magic",Chinesename:"魔法",Phonetic:"ˋmædʒɪk",image:"magic.jpg"),
        R_letter(name:"magnifier",Chinesename:"放大鏡",Phonetic:"ˋmægnə͵faɪɚ",image:"magnifier.png"),
        R_letter(name:"mail",Chinesename:"郵件",Phonetic:"mel",image:"mail.png"),
        R_letter(name:"mailbox",Chinesename:"郵筒",Phonetic:"ˋmel͵bɑks",image:"mailbox.jpg"),
        R_letter(name:"mailman",Chinesename:"郵差",Phonetic:"ˋmel͵mæn",image:"mailman.jpg"),
        R_letter(name:"mainmast",Chinesename:"主桅",Phonetic:"ˋmen͵mæst",image:"mainmast.jpg"),
        R_letter(name:"maize",Chinesename:"玉蜀黍",Phonetic:"mez",image:"maize.jpg"),
        R_letter(name:"male",Chinesename:"男子",Phonetic:"mel",image:"male.png"),
        R_letter(name:"malefactor",Chinesename:"罪人",Phonetic:"ˋmælə͵fæktɚ",image:"malefactor.jpg"),
        R_letter(name:"malt",Chinesename:"啤酒",Phonetic:"mɔlt",image:"malt.jpg"),
        R_letter(name:"mango",Chinesename:"芒果",Phonetic:"ˋmæŋgo",image:"mango.jpg"),
        R_letter(name:"manor",Chinesename:"莊園",Phonetic:"ˋmænɚ",image:"manor.jpg"),
        R_letter(name:"manservant",Chinesename:"女僕",Phonetic:"ˋmæn͵sɝvənt",image:"manservant.png"),
        R_letter(name:"manure",Chinesename:"肥料",Phonetic:"məˋnjʊr",image:"manure.jpg"),
        R_letter(name:"maple",Chinesename:"楓樹",Phonetic:"ˋmep!",image:"maple.jpg"),
        R_letter(name:"marble",Chinesename:"大理石",Phonetic:"ˋmɑrb!",image:"marble.jpg"),
        R_letter(name:"mariner",Chinesename:"水手",Phonetic:"ˋmærənɚ",image:"mariner.png"),
        R_letter(name:"marionette",Chinesename:"牽線木偶",Phonetic:"͵mærɪəˋnɛt",image:"marionette.jpg"),
        R_letter(name:"marish",Chinesename:"沼澤",Phonetic:"ˋmærɪʃ",image:"marish.jpg"),
        R_letter(name:"R_letterm",Chinesename:"母親",Phonetic:"mɑm",image:"R_letterm.jpg"),
        R_letter(name:"narrow",Chinesename:"狹隘的;狹窄的",Phonetic:"ˋnæro",image:"narrow.jpg"),
        R_letter(name:"nearsighted",Chinesename:"近視的",Phonetic:"ˋnɪrˋsaɪtɪd",image:"nearsighted.jpg"),
        R_letter(name:"neat",Chinesename:"整潔的",Phonetic:"nit",image:"neat.jpg"),
        R_letter(name:"necessary",Chinesename:"必要的",Phonetic:"ˋnɛsə͵sɛrɪ",image:"necessary.png"),
        R_letter(name:"needy",Chinesename:"貧窮的",Phonetic:"ˋnidɪ",image:"needy.jpg"),
        R_letter(name:"neglect",Chinesename:"忽視",Phonetic:"nɪgˋlɛkt",image:"neglect.jpg"),
        R_letter(name:"negligence",Chinesename:"粗心",Phonetic:"ˋnɛglɪdʒəns",image:"negligence.jpg"),
        R_letter(name:"negligible",Chinesename:"微不足道的",Phonetic:"ˋnɛglɪdʒəb!",image:"negligible.png"),
        R_letter(name:"negotiable",Chinesename:"可協商的",Phonetic:"nɪˋgoʃɪəb!",image:"negotiable.jpg"),
        R_letter(name:"neighborly",Chinesename:"親切的",Phonetic:"ˋnebɚlɪ",image:"neighborly.jpg"),
        R_letter(name:"nephew",Chinesename:"姪兒",Phonetic:"ˋnɛfju",image:"nephew.jpg"),
        R_letter(name:"nerve",Chinesename:"神經",Phonetic:"nɝv",image:"nerve.jpg"),
        R_letter(name:"nevertheless",Chinesename:"仍然",Phonetic:"͵nɛvɚðəˋlɛs",image:"nevertheless.png"),
        R_letter(name:" newlywed",Chinesename:"新結婚的人",Phonetic:"ˋnjulɪ͵wɛd",image:"newlywed.jpg"),
        R_letter(name:"nickel",Chinesename:"鎳",Phonetic:"ˋnɪk!",image:"nickel.jpg"),
        R_letter(name:"nifty",Chinesename:"俏皮的",Phonetic:"ˋnɪftɪ",image:"nifty.jpg"),
        R_letter(name:"nimble",Chinesename:"靈活的",Phonetic:"ˋnɪmb!",image:"nimble.jpg"),
        R_letter(name:"nine",Chinesename:"九",Phonetic:"naɪn",image:"nine.jpg"),
        R_letter(name:"nitrogen",Chinesename:"氮",Phonetic:"ˋnaɪtrədʒən",image:"nitrogen.png"),
        R_letter(name:"noble",Chinesename:"高貴的",Phonetic:"ˋnob!",image:"noble.png"),
        R_letter(name:"nocturne",Chinesename:"夜曲",Phonetic:"ˋnɑktɝn",image:"nocturne.jpg"),
        R_letter(name:"nominal",Chinesename:"名義上的",Phonetic:"ˋnɑmən!",image:"nominal.jpg"),
        R_letter(name:"nominate",Chinesename:"題名",Phonetic:"ˋnɑmə͵net",image:"nominate.jpg"),
        R_letter(name:"notable",Chinesename:"顯著的",Phonetic:"ˋnotəb!",image:"notable.jpg"),
        R_letter(name:"notorious",Chinesename:"惡名昭彰的",Phonetic:"noˋtorɪəs",image:"notorious.png"),
        R_letter(name:"novelty",Chinesename:"新穎",Phonetic:"ˋnɑv!tɪ",image:"novelty.jpg"),
        R_letter(name:"nucleus",Chinesename:"原子核",Phonetic:"ˋnjuklɪəs",image:"nucleus.jpg"),
        R_letter(name:"nuisance",Chinesename:"麻煩事",Phonetic:"ˋnjusns",image:"nuisance.jpg"),
        R_letter(name:"nursery",Chinesename:"幼兒室",Phonetic:"ˋnɝsərɪ",image:"nursery.jpg"),
        R_letter(name:"nutritionist",Chinesename:"營養學家",Phonetic:"njuˋtrɪʃənɪst",image:"nutritionist.jpg"),
        R_letter(name:"oak",Chinesename:"橡樹",Phonetic:"oʊk",image:"oak.jpg"),
        R_letter(name:"oar",Chinesename:"槳",Phonetic:"ɔr, or",image:"oar.jpg"),
        R_letter(name:"oasis",Chinesename:"綠洲",Phonetic:"oʊˈeɪsɪs",image:"oasis.jpg"),
        R_letter(name:"oath",Chinesename:"宣誓",Phonetic:"oʊθ",image:"oath.jpg"),
        R_letter(name:"obey",Chinesename:"服從",Phonetic:"oˈbe",image:"obey.jpg"),
        R_letter(name:"obituary",Chinesename:"死亡公告",Phonetic:"oʊˈbɪtʃueri",image:"obituary.jpg"),
        R_letter(name:"object",Chinesename:"物件",Phonetic:"ˈɑbdʒekt",image:"object.jpg"),
        R_letter(name:"obscure",Chinesename:"不清楚的",Phonetic:"əbˈskjʊr",image:"obscure.jpg"),
        R_letter(name:"observatory",Chinesename:"觀測所",Phonetic:"əbˈzɜrvətɔri",image:"observatory.jpg"),
        R_letter(name:"obstacle",Chinesename:"障礙物",Phonetic:"ˈɑbstəkl",image:"obstacle.jpg"),
        R_letter(name:"octopus",Chinesename:"章魚",Phonetic:"ˈɑktəpəs]",image:"octopus.jpg"),
        R_letter(name:"office",Chinesename:"辦公室",Phonetic:"ˈɔfɪs",image:"office.jpg"),
        R_letter(name:"officer",Chinesename:"軍官,官吏,警官,警察",Phonetic:"ˈɔfɪsə(r)",image:"officer.jpg"),
        R_letter(name:"onion",Chinesename:"洋蔥",Phonetic:"ˈʌnjən",image:"onion.jpg"),
        R_letter(name:"op",Chinesename:"開刀手術",Phonetic:"ɑp",image:"op.jpg"),
        R_letter(name:"opener",Chinesename:"開瓶器,開罐器",Phonetic:"ˈəʊpnə(r)",image:"opener.jpg"),
        R_letter(name:"opera",Chinesename:"歌劇",Phonetic:"ˈɑprə",image:"opera.jpg"),
        R_letter(name:"oppose",Chinesename:"反對",Phonetic:"əˈpoʊz",image:"oppose.jpg"),
        R_letter(name:"orchestra",Chinesename:"管弦樂隊",Phonetic:"ˈɔ:kɪstrə",image:"orchestra.jpg"),
        R_letter(name:"ore",Chinesename:"礦石",Phonetic:"ɔr",image:"ore.jpg"),
        R_letter(name:"organ",Chinesename:"器官,風琴",Phonetic:"ˈɔ:gən",image:"organ.jpg"),
        R_letter(name:"oscillator",Chinesename:"震盪器",Phonetic:"ˈɒsɪleɪtə(r)",image:"oscillator.jpg"),
        R_letter(name:"ostrich",Chinesename:"鴕鳥",Phonetic:"ˈɒstrɪtʃ",image:"ostrich.jpg"),
        R_letter(name:"outbreak",Chinesename:"暴動",Phonetic:"ˈaʊtbreɪk",image:"outbreak.jpg"),
        R_letter(name:"outskirts",Chinesename:"郊區",Phonetic:"ˈaʊtskɜ:ts",image:"outskirts.jpg"),
        R_letter(name:"oven",Chinesename:"烤箱",Phonetic:"ˈʌvn",image:"oven.jpg"),
        R_letter(name:"overlook",Chinesename:"俯視",Phonetic:"ˌəʊvəˈlʊk",image:"overlook.jpg"),
        R_letter(name:"overpass",Chinesename:"天橋",Phonetic:"ˈəʊvəpɑ:s",image:"overpass.jpg"),
        R_letter(name:"owl",Chinesename:"貓頭鷹",Phonetic:"aʊl",image:"owl.jpg"),
        R_letter(name:"ox",Chinesename:"公牛",Phonetic:"ɒks",image:"ox.jpg"),
        R_letter(name:"packman",Chinesename:"小販",Phonetic:"ˋpækmən",image:"packman.jpg"),
        R_letter(name:"paction",Chinesename:"契約書",Phonetic:"ˋpækʃən",image:"paction.jpg"),
        R_letter(name:"padre",Chinesename:"神父",Phonetic:"ˋpɑdrɪ",image:"padre.jpg"),
        R_letter(name:"palace",Chinesename:"宮殿",Phonetic:"ˋpælɪs",image:"palace.jpg"),
        R_letter(name:"pamphlet",Chinesename:"小冊子",Phonetic:"ˋpæmflɪt",image:"pamphlet.jpg"),
        R_letter(name:"pan",Chinesename:"平底鍋",Phonetic:"pæn",image:"pan.jpg"),
        R_letter(name:"pancake",Chinesename:"鬆餅",Phonetic:"ˋpæn͵kek",image:"pancake.jpg"),
        R_letter(name:"panda",Chinesename:"貓熊",Phonetic:"ˋpændə",image:"panda.jpg"),
        R_letter(name:"parakeet",Chinesename:"鸚鵡",Phonetic:"ˋpærə͵kit",image:"parakeet.jpg"),
        R_letter(name:"parasol",Chinesename:"陽傘",Phonetic:"ˋpærə͵sɔl",image:"parasol.jpg"),
        R_letter(name:"parcel",Chinesename:"包裹",Phonetic:"ˋpɑrs!",image:"parcel.jpg"),
        R_letter(name:"pard",Chinesename:"豹",Phonetic:"pɑrd",image:"pard.jpg"),
        R_letter(name:"partridge",Chinesename:"鷓鴣",Phonetic:"ˋpɑrtrɪdʒ",image:"partridge.jpg"),
        R_letter(name:"passnameer",Chinesename:"乘客",Phonetic:"ˋpæsndʒɚ",image:"passnameer.png"),
        R_letter(name:"pastry",Chinesename:"糕點",Phonetic:"ˋpestrɪ",image:"pastry.jpg"),
        R_letter(name:"pasture",Chinesename:"牧場",Phonetic:"ˋpæstʃɚ",image:"pasture.jpg"),
        R_letter(name:"patio",Chinesename:"露臺",Phonetic:"ˋpɑtɪ͵o",image:"patio.jpg"),
        R_letter(name:"pear",Chinesename:"梨",Phonetic:"pɛr",image:"pear.jpg"),
        R_letter(name:"pedal",Chinesename:"踏板",Phonetic:"ˋpɛd!",image:"pedal.jpg"),
        R_letter(name:"peddler",Chinesename:"小販",Phonetic:"ˋpɛdlɚ",image:"peddler.jpg"),
        R_letter(name:"pencil",Chinesename:"鉛筆",Phonetic:"ˋpɛns!",image:"pencil.png"),
        R_letter(name:"pnameuin",Chinesename:"企鵝",Phonetic:"ˋpɛngwɪn",image:"pnameuin.jpg"),
        R_letter(name:"peony",Chinesename:"牡丹",Phonetic:"ˋpiənɪ",image:"peony.jpg"),
        R_letter(name:"periwig",Chinesename:"假髮",Phonetic:"ˋpɛrə͵wɪg",image:"periwig.jpg"),
        R_letter(name:"permit",Chinesename:"執照",Phonetic:"pɚˋmɪt",image:"permit.jpg"),
        R_letter(name:"persimR_lettern",Chinesename:"柿子",Phonetic:"pɚˋsɪmən",image:"persimR_lettern.jpg"),
        R_letter(name:"pesticide",Chinesename:"殺蟲劑",Phonetic:"ˋpɛstɪ͵saɪd",image:"pesticide.jpg"),
        R_letter(name:"petal",Chinesename:"花瓣",Phonetic:"ˋpɛt!",image:"petal.jpg"),
        R_letter(name:"petrel",Chinesename:"海燕",Phonetic:"ˋpɛtrəl",image:"petrel.jpg"),
        R_letter(name:"petroleum",Chinesename:"石油",Phonetic:"pəˋtrolɪəm",image:"petroleum.jpg"),
        R_letter(name:"quack",Chinesename:"庸醫",Phonetic:"kwæk",image:"quack.jpg"),
        R_letter(name:"quadrangle",Chinesename:"四邊形",Phonetic:"ˋkwɑdræŋg!",image:"quadrangle.png"),
        R_letter(name:"quagmire",Chinesename:"沼澤地",Phonetic:"ˋkwæg͵maɪr",image:"quagmire.jpg"),
        R_letter(name:"quail",Chinesename:"鵪鶉",Phonetic:"kwel",image:"quail.jpg"),
        R_letter(name:"quaint",Chinesename:"古色古香的",Phonetic:"kweɪnt",image:"quaint.jpg"),
        R_letter(name:"quake",Chinesename:"地震",Phonetic:"kwek",image:"quake.jpg"),
        R_letter(name:"qualification",Chinesename:"執照",Phonetic:"͵kwɑləfəˋkeʃən",image:"qualification.jpg"),
        R_letter(name:"qualm",Chinesename:"不安",Phonetic:"kwɔm",image:"qualm.jpg"),
        R_letter(name:"quandary",Chinesename:"困惑",Phonetic:"ˋkwɑndərɪ",image:"quandary.jpg"),
        R_letter(name:"quarantine",Chinesename:"隔離",Phonetic:"ˋkwɔrən͵tin",image:"quarantine.png"),
        R_letter(name:"quarrel",Chinesename:"爭吵",Phonetic:"ˋkwɔrəl",image:"quarrel.jpg"),
        R_letter(name:"quarry",Chinesename:"採石場",Phonetic:"ˋkwɔrɪ",image:"quarry.jpg"),
        R_letter(name:"quarter",Chinesename:"住所",Phonetic:"ˋkwɔrtɚ",image:"quarter.jpg"),
        R_letter(name:"quartermaster",Chinesename:"軍需官",Phonetic:"ˋkwɔrtɚ͵mæstɚ",image:"quartermaster.jpg"),
        R_letter(name:"quartet",Chinesename:"四重奏",Phonetic:"kwɔrˋtɛt",image:"quartet.png"),
        R_letter(name:"quartz",Chinesename:"石英",Phonetic:"kwɔrts",image:"quartz.jpg"),
        R_letter(name:"quasar",Chinesename:"類星體",Phonetic:"ˋkwesɑr",image:"quasar.jpg"),
        R_letter(name:"quaver",Chinesename:"震動",Phonetic:"ˋkwevɚ",image:"quaver.jpg"),
        R_letter(name:"quay",Chinesename:"碼頭",Phonetic:"ki",image:"quay.jpg"),
        R_letter(name:"queen",Chinesename:"女皇",Phonetic:"ˋkwin",image:"queen.png"),
        R_letter(name:"quell",Chinesename:"鎮壓",Phonetic:"kwɛl",image:"quell.jpg"),
        R_letter(name:"quench",Chinesename:"熄滅",Phonetic:"kwɛntʃ",image:"quench.jpg"),
        R_letter(name:"quest",Chinesename:"尋找",Phonetic:"kwɛst",image:"quest.jpg"),
        R_letter(name:"questionable",Chinesename:"可疑的",Phonetic:"ˋkwɛstʃənəb!",image:"questionable.jpg"),
        R_letter(name:"questionnaire",Chinesename:"問卷",Phonetic:"͵kwɛstʃənˋɛr",image:"questionnaire.jpg"),
        R_letter(name:"queue",Chinesename:"列隊",Phonetic:"kju",image:"queue.jpg"),
        R_letter(name:"quicksand",Chinesename:"流沙",Phonetic:"ˋkwɪk͵sænd",image:"quicksand.jpg"),
        R_letter(name:"quicksilver",Chinesename:"水銀",Phonetic:"ˋkwɪk͵sɪlvɚ",image:"quicksilver.jpg"),
        R_letter(name:"quilt",Chinesename:"被子",Phonetic:"kwɪlt",image:"quilt.jpg"),
        R_letter(name:"quisling",Chinesename:"內奸",Phonetic:"ˋkwɪz!ɪŋ",image:"quisling.jpg"),
        R_letter(name:"rabbit",Chinesename:"兔子",Phonetic:"ˈrabət",image:"rabbit.jpg"),
        R_letter(name:"raccoon",Chinesename:"浣熊",Phonetic:"raˈko͞on",image:"raccoon.jpg"),
        R_letter(name:"racket",Chinesename:"球拍",Phonetic:"ˈrakət",image:"racket.jpg"),
        R_letter(name:"radish",Chinesename:"小蘿蔔",Phonetic:"ˈradiSH",image:"radish.jpg"),
        R_letter(name:"railway",Chinesename:"鐵路",Phonetic:"ˈrālˌwā",image:"railway.jpg"),
        R_letter(name:"rainbow",Chinesename:"彩虹",Phonetic:"ˈrānˌbō",image:"rainbow.jpg"),
        R_letter(name:"rainbow trout",Chinesename:"虹鱒魚",Phonetic:"ˈˌrānbō ˈtrout",image:"rainbow trout.jpg"),
        R_letter(name:"raisin",Chinesename:"葡萄乾",Phonetic:"ˈrāzən",image:"raisin.jpg"),
        R_letter(name:"ram",Chinesename:"公羊",Phonetic:"ram",image:"ram.jpg"),
        R_letter(name:"razorback",Chinesename:"野豬",Phonetic:"ˈrāzər,bak",image:"razorback.jpg"),
        R_letter(name:"red squirrel",Chinesename:"紅松鼠",Phonetic:"red ˈskwər(ə)l",image:"red squirrel.jpg"),
        R_letter(name:"redbird",Chinesename:"紅雀",Phonetic:"ˈredbə:d",image:"redbird.jpg"),
        R_letter(name:"redbreast",Chinesename:"知更鳥",Phonetic:"ˈredˌbrest",image:"redbreast.jpg"),
        R_letter(name:"redhead",Chinesename:"紅頭鴨",Phonetic:"ˈredˌhed",image:"redhead.jpg"),
        R_letter(name:"redwing",Chinesename:"紅翼鶇",Phonetic:"ˈredwiNG",image:"redwing.jpg"),
        R_letter(name:"refrigerator",Chinesename:"冰箱",Phonetic:"rəˈfrijəˌrādər",image:"refrigerator.jpg"),
        R_letter(name:"reindeer",Chinesename:"馴鹿",Phonetic:"ˈrānˌdi",image:"reindeer.jpg"),
        R_letter(name:"reservoir",Chinesename:"水庫",Phonetic:"ˈrezər,vwär",image:"reservoir.jpg"),
        R_letter(name:"restaurant",Chinesename:"餐廳",Phonetic:"ˈrest(ə)rənt",image:"restaurant.jpg"),
        R_letter(name:"rhinoceros",Chinesename:"犀牛",Phonetic:"rīˈnäs(ə)rəs",image:"rhinoceros.jpg"),
        R_letter(name:"rice",Chinesename:"米飯",Phonetic:"rīs",image:"rice.jpg"),
        R_letter(name:"rick",Chinesename:"乾草堆",Phonetic:"rik",image:"rick.jpg"),
        R_letter(name:"ringdove",Chinesename:"斑鳩",Phonetic:"ˈriNGdəv",image:"ringdove.jpg"),
        R_letter(name:"rink",Chinesename:"溜冰場",Phonetic:"riNGk",image:"rink.jpg"),
        R_letter(name:"river",Chinesename:"河",Phonetic:"ˈrivər",image:"river.jpg"),
        R_letter(name:"robot",Chinesename:"機器人",Phonetic:"ˈrōbət",image:"robot.jpg"),
        R_letter(name:"roebuck",Chinesename:"雄鹿",Phonetic:"ˈrōˌbək",image:"roebuck.jpg"),
        R_letter(name:"rooster",Chinesename:"公雞",Phonetic:"ˈro͞ostər",image:"rooster.jpg"),
        R_letter(name:"rose",Chinesename:"玫瑰花",Phonetic:"rōz",image:"rose.jpg"),
        R_letter(name:"rosemary",Chinesename:"迷迭香",Phonetic:"ˈrōz,merē",image:"rosemary.jpg"),
        R_letter(name:"saber",Chinesename:"軍刀",Phonetic:"ˋsebɚ",image:"saber.jpg"),
        R_letter(name:"sable",Chinesename:"黑貂",Phonetic:"ˋseb!",image:"sable.jpg"),
        R_letter(name:"sachem",Chinesename:"酋長",Phonetic:"ˋsetʃəm",image:"sachem.jpg"),
        R_letter(name:"sacrament",Chinesename:"聖禮",Phonetic:"ˋsækrəmənt",image:"sacrament.jpg"),
        R_letter(name:"saddle",Chinesename:"馬鞍",Phonetic:"ˋsæd!",image:"saddle.jpg"),
        R_letter(name:"sailboat",Chinesename:"帆船",Phonetic:"ˋsel͵bot",image:"sailboat.jpg"),
        R_letter(name:"sailor",Chinesename:"水手",Phonetic:"ˋselɚ",image:"sailor.jpg"),
        R_letter(name:"salacity",Chinesename:"好色",Phonetic:"səˋlæsətɪ",image:"salacity.jpg"),
        R_letter(name:"salad",Chinesename:"沙拉",Phonetic:"ˋsæləd",image:"salad.jpg"),
        R_letter(name:"salamander",Chinesename:"火蜥蜴",Phonetic:"ˋsælə͵mændɚ",image:"salamander.jpg"),
        R_letter(name:"salary",Chinesename:"薪水",Phonetic:"ˋsælərɪ",image:"salary.jpg"),
        R_letter(name:"sale",Chinesename:"銷售",Phonetic:"sel",image:"sale.jpg"),
        R_letter(name:"salt",Chinesename:"鹽",Phonetic:"sɔlt",image:"salt.jpg"),
        R_letter(name:"saltpeter",Chinesename:"硝石",Phonetic:"ˋsɔlt͵pitɚ",image:"saltpeter.jpg"),
        R_letter(name:"samisen",Chinesename:"三弦琴",Phonetic:"ˋsæmə͵sɛn",image:"samisen.jpg"),
        R_letter(name:"sanctuary",Chinesename:"聖堂",Phonetic:"ˋsæŋktʃʊ͵ɛrɪ",image:"sanctuary.jpg"),
        R_letter(name:"sand",Chinesename:"沙",Phonetic:"sænd",image:"sand.jpg"),
        R_letter(name:"sap",Chinesename:"樹液",Phonetic:"sæp",image:"sap.jpg"),
        R_letter(name:"sarcoma",Chinesename:"肉瘤",Phonetic:"sɑrˋkomə",image:"sarcoma.jpg"),
        R_letter(name:"sarcophagus",Chinesename:"石棺",Phonetic:"sɑrˋkɑfəgəs",image:"sarcophagus.jpg"),
        R_letter(name:"sardine",Chinesename:"沙丁魚",Phonetic:"sɑrˋdin",image:"sardine.jpg"),
        R_letter(name:"sargasso",Chinesename:"馬尾藻",Phonetic:"sɑrˋgæso",image:"sargasso.jpg"),
        R_letter(name:"satchel",Chinesename:"書包",Phonetic:"ˋsætʃəl",image:"satchel.jpg"),
        R_letter(name:"Saturn",Chinesename:"土星",Phonetic:"ˋsætɚn",image:"Saturn.jpg"),
        R_letter(name:"sausage",Chinesename:"香腸",Phonetic:"ˋsɔsɪdʒ",image:"sausage.jpg"),
        R_letter(name:"scabbard",Chinesename:"槍套",Phonetic:"ˋskæbɚd",image:"scabbard.jpg"),
        R_letter(name:"scale",Chinesename:"天秤",Phonetic:"skel",image:"scale.jpg"),
        R_letter(name:"scallop",Chinesename:"扇貝",Phonetic:"ˋskɑləp",image:"scallop.jpg"),
        R_letter(name:"slaver",Chinesename:"盤子",Phonetic:"ˋslevɚ",image:"slaver.jpg"),
        R_letter(name:"surf",Chinesename:"海浪",Phonetic:"sɝf",image:"surf.jpg"),
        R_letter(name:"Tablespoon",Chinesename:"大湯匙",Phonetic:"ˈtābəlˌspo͞on",image:"tablespoon.jpg"),
        R_letter(name:"Tablet",Chinesename:"平板",Phonetic:"ˈtablət",image:"tablet.jpg"),
        R_letter(name:"Tailor",Chinesename:"裁縫師",Phonetic:"ˈtālər",image:"tailor.jpg"),
        R_letter(name:"Tale",Chinesename:"故事",Phonetic:"tāl",image:"tale.jpg"),
        R_letter(name:"Tangerine",Chinesename:"橘子",Phonetic:"ˌtanjəˈrēn",image:"tangerine.jpg"),
        R_letter(name:"Tanker",Chinesename:"油輪",Phonetic:"ˈtaNGkər",image:"tanker.jpg"),
        R_letter(name:"Technician",Chinesename:"技術人員",Phonetic:"tekˈniSHən",image:"technician.jpg"),
        R_letter(name:"Teenager",Chinesename:"青少年",Phonetic:"ˈtēnˌājər",image:"teenager.png"),
        R_letter(name:"Telegram",Chinesename:"電報",Phonetic:"telegram",image:"telegram.jpg"),
        R_letter(name:"Telescope",Chinesename:"望遠鏡",Phonetic:"ˈteləˌskōp",image:"telescope.jpg"),
        R_letter(name:"Television",Chinesename:"電視",Phonetic:"ˈteləˌviZHən",image:"television.jpg"),
        R_letter(name:"Temperature",Chinesename:"溫度",Phonetic:"ˈtemp(ə)rəCHər",image:"temperature.jpg"),
        R_letter(name:"Throat",Chinesename:"喉嚨",Phonetic:"THrōt",image:"throat.jpg"),
        R_letter(name:"Thunderstorm",Chinesename:"大雷雨",Phonetic:"ˈTHəndərˌstôrm",image:"thunderstorm.jpg"),
        R_letter(name:"Tissue",Chinesename:"衛生紙",Phonetic:"ˈtiSHo͞o",image:"tissue.jpg"),
        R_letter(name:"Topics",Chinesename:"熱帶",Phonetic:"ˈtäpik",image:"topics.jpg"),
        R_letter(name:"Tourist",Chinesename:"觀光客",Phonetic:"ˈto͝orəst",image:"tourist.jpg"),
        R_letter(name:"Township",Chinesename:"鄉",Phonetic:"ˈtounˌSHip",image:"township.jpg"),
        R_letter(name:"Track",Chinesename:"軌道",Phonetic:"trak",image:"track.jpg"),
        R_letter(name:"Trademark",Chinesename:"商標",Phonetic:"ˈtrādˌmärk",image:"trademark.jpg"),
        R_letter(name:"Transfusion",Chinesename:"輸血",Phonetic:"ˌtran(t)sˈfyo͞oZHən",image:"transfusion.jpg"),
        R_letter(name:"Translator",Chinesename:"翻譯機",Phonetic:"ˈtransˌlādər",image:"translator.jpg"),
        R_letter(name:"Tribe",Chinesename:"部落",Phonetic:"trīb",image:"tribe.jpg"),
        R_letter(name:"Trolley",Chinesename:"手推車",Phonetic:"ˈträlē",image:"trolley.jpg"),
        R_letter(name:"Trousers",Chinesename:"褲子",Phonetic:"ˈtrouzərz",image:"trousers.jpg"),
        R_letter(name:"Trout",Chinesename:"鮭魚",Phonetic:"trout",image:"trout.jpg"),
        R_letter(name:"Trumpet",Chinesename:"喇叭",Phonetic:"ˈtrəmpət",image:"trumpet.jpg"),
        R_letter(name:"Tuxedo",Chinesename:"燕尾服",Phonetic:"təkˈsēdō",image:"tuxedo.jpg"),
        R_letter(name:"Typhoon",Chinesename:"颱風",Phonetic:"tīˈfo͞on",image:"typhoon.jpg"),
        R_letter(name:"Tyre",Chinesename:"輪胎",Phonetic:"tī(ə)r",image:"tyre.jpg"),
        R_letter(name:"umbrella",Chinesename:"雨傘",Phonetic:"ʌmˋbrɛlə",image:"umbrella.jpg"),
        R_letter(name:"ump",Chinesename:"裁判",Phonetic:"ʌmp",image:"ump.jpg"),
        R_letter(name:"uncle",Chinesename:"叔叔",Phonetic:"ˋʌŋk!",image:"uncle.jpg"),
        R_letter(name:"undershirt",Chinesename:"汗衫",Phonetic:"ˋʌndɚ͵ʃɝt",image:"undershirt.jpg"),
        R_letter(name:"undertaker",Chinesename:"企業家",Phonetic:"͵ʌndɚˋtekɚ",image:"undertaker.jpg"),
        R_letter(name:"undoing",Chinesename:"毀滅",Phonetic:"ʌnˋduɪŋ",image:"undoing.jpg"),
        R_letter(name:"unguent",Chinesename:"藥膏",Phonetic:"ˋʌŋgwənt",image:"unguent.jpg"),
        R_letter(name:"unicorn",Chinesename:"獨角獸",Phonetic:"ˋjunɪ͵kɔrn",image:"unicorn.png"),
        R_letter(name:"universe",Chinesename:"宇宙",Phonetic:"ˋjunə͵vɝs",image:"universe.jpg"),
        R_letter(name:"university",Chinesename:"大學",Phonetic:"͵junəˋvɝsətɪ",image:"university.jpg"),
        R_letter(name:"untruth",Chinesename:"謊言",Phonetic:"ʌnˋtruθ",image:"untruth.jpg"),
        R_letter(name:"update",Chinesename:"更新",Phonetic:"ʌpˋdet",image:"update.jpg"),
        R_letter(name:"upland",Chinesename:"高地",Phonetic:"ˋʌplənd",image:"upland.jpg"),
        R_letter(name:"upraise",Chinesename:"舉起",Phonetic:"ʌpˋrez",image:"upraise.jpg"),
        R_letter(name:"uprear",Chinesename:"扶養",Phonetic:"ʌpˋrɪr",image:"uprear.jpg"),
        R_letter(name:"uprising",Chinesename:"叛亂",Phonetic:"ˋʌp͵raɪzɪŋ",image:"uprising.jpg"),
        R_letter(name:"uproar",Chinesename:"擾亂",Phonetic:"ˋʌp͵ror",image:"uproar.jpg"),
        R_letter(name:"upset",Chinesename:"難過的",Phonetic:"ʌpˋsɛt",image:"upset.jpg"),
        R_letter(name:"upshot",Chinesename:"結尾",Phonetic:"ˋʌp͵ʃɑt",image:"upshot.jpg"),
        R_letter(name:"upswing",Chinesename:"上升",Phonetic:"ˋʌp͵swɪŋ",image:"upswing.jpg"),
        R_letter(name:"uptown",Chinesename:"住宅區",Phonetic:"ˋʌpˋtaʊn",image:"uptown.jpg"),
        R_letter(name:"uranium",Chinesename:"軸",Phonetic:"jʊˋrenɪəm",image:"uranium.jpg"),
        R_letter(name:"Uranus",Chinesename:"天王星",Phonetic:"ˋjʊərənəs",image:"Uranus.jpg"),
        R_letter(name:"urchin",Chinesename:"小孩",Phonetic:"ˋɝtʃɪn",image:"urchin.png"),
        R_letter(name:"urinal",Chinesename:"尿壺",Phonetic:"ˋjʊrən!",image:"urinal.jpg"),
        R_letter(name:"urn",Chinesename:"甕",Phonetic:"ɝn",image:"urn.jpg"),
        R_letter(name:"user",Chinesename:"用戶",Phonetic:"ˋjuzɚ",image:"user.png"),
        R_letter(name:"usher",Chinesename:"招待員",Phonetic:"ˋʌʃɚ",image:"usher.jpg"),
        R_letter(name:"ute",Chinesename:"載貨汽車",Phonetic:"juːt",image:"ute.jpg"),
        R_letter(name:"uterus",Chinesename:"子宮",Phonetic:"ˋjutərəs",image:"uterus.jpg"),
        R_letter(name:"valentine",Chinesename:"情人",Phonetic:"ˋvæləntaɪn",image:"valentine.jpg"),
        R_letter(name:"valise",Chinesename:"旅行袋",Phonetic:"vəˋlis",image:"valise.jpg"),
        R_letter(name:"valley",Chinesename:"山谷",Phonetic:"ˋvælɪ",image:"valley.jpg"),
        R_letter(name:"vampire",Chinesename:"吸血鬼",Phonetic:"ˋvæmpaɪr",image:"vampire.jpg"),
        R_letter(name:"van",Chinesename:"運貨車",Phonetic:"væn",image:"van.jpg"),
        R_letter(name:"vanguard",Chinesename:"先鋒",Phonetic:"ˋvæn͵gɑrd",image:"vanguard.jpg"),
        R_letter(name:"vanilla",Chinesename:"香草",Phonetic:"vəˋnɪlə",image:"vanilla.jpg"),
        R_letter(name:"vapor",Chinesename:"蒸氣",Phonetic:"ˋvepɚ",image:"vapor.jpg"),
        R_letter(name:"varlet",Chinesename:"惡棍",Phonetic:"ˋvɑrlɪt",image:"varlet.jpg"),
        R_letter(name:"vaseline",Chinesename:"凡士林",Phonetic:"ˋvæs!͵in",image:"vaseline.jpg"),
        R_letter(name:"vassal",Chinesename:"諸侯",Phonetic:"ˋvæs!",image:"vassal.jpg"),
        R_letter(name:"vaudeville",Chinesename:"雜耍",Phonetic:"ˋvodə͵vɪl",image:"vaudeville.jpg"),
        R_letter(name:"vegetable",Chinesename:"蔬菜",Phonetic:"ˋvɛdʒətəb!",image:"vegetable.jpg"),
        R_letter(name:"vegetation",Chinesename:"植物",Phonetic:"͵vɛdʒəˋteʃən",image:"vegetation.jpg"),
        R_letter(name:"veil",Chinesename:"面紗",Phonetic:"vel",image:"veil.jpg"),
        R_letter(name:"vein",Chinesename:"靜脈",Phonetic:"ven",image:"vein.jpg"),
        R_letter(name:"velvet",Chinesename:"絲絨",Phonetic:"ˋvɛlvɪt",image:"velvet.jpg"),
        R_letter(name:"vendor",Chinesename:"小販",Phonetic:"ˋvɛndɚ",image:"vendor.png"),
        R_letter(name:"vest",Chinesename:"馬甲",Phonetic:"vɛst",image:"vest.jpg"),
        R_letter(name:"vestment",Chinesename:"官服",Phonetic:"ˋvɛstmənt",image:"vestment.jpg"),
        R_letter(name:"vesture",Chinesename:"衣服",Phonetic:"ˋvɛstʃɚ",image:"vesture.jpg"),
        R_letter(name:"vetch",Chinesename:"野豌豆",Phonetic:"vɛtʃ",image:"vetch.jpg"),
        R_letter(name:"viaduct",Chinesename:"高架橋",Phonetic:"ˋvaɪə͵dʌkt",image:"viaduct.jpg"),
        R_letter(name:"villa",Chinesename:"別墅",Phonetic:"ˋvɪlə",image:"villa.jpg"),
        R_letter(name:"villain",Chinesename:"壞人",Phonetic:"ˋvɪlən",image:"villain.png"),
        R_letter(name:"vinegar",Chinesename:"醋",Phonetic:"ˋvɪnɪgɚ",image:"vinegar.jpg"),
        R_letter(name:"viola",Chinesename:"中提琴",Phonetic:"vɪˋolə",image:"viola.jpg"),
        R_letter(name:"violin",Chinesename:"小提琴",Phonetic:"͵vaɪəˋlɪn",image:"violin.jpg"),
        R_letter(name:"violoncello",Chinesename:"大提琴",Phonetic:"͵viəlɑnˋtʃɛlo",image:"violoncello.jpg"),
        R_letter(name:"volcano",Chinesename:"火山",Phonetic:"vɑlˋkeno",image:"volcano.jpg"),
        R_letter(name:"wafer",Chinesename:"晶片",Phonetic:"ˋwefɚ",image:"wafer.jpg"),
        R_letter(name:"waft",Chinesename:"漂浮",Phonetic:"wæft",image:"waft.jpg"),
        R_letter(name:"wag",Chinesename:"搖擺",Phonetic:"wæg",image:"wag.jpg"),
        R_letter(name:"wage",Chinesename:"工資",Phonetic:"wedʒ",image:"wage.jpg"),
        R_letter(name:"wager",Chinesename:"賭",Phonetic:"ˋwedʒɚ",image:"wager.jpg"),
        R_letter(name:"wagon",Chinesename:"敞篷車廂",Phonetic:"ˋwægən",image:"wagon.jpg"),
        R_letter(name:"wagoner",Chinesename:"車夫",Phonetic:"ˋwægənɚ",image:"wagoner.jpg"),
        R_letter(name:"waist",Chinesename:"腰",Phonetic:"west",image:"waist.jpg"),
        R_letter(name:"waistcoat",Chinesename:"背心",Phonetic:"ˋwest͵kot",image:"waistcoat.jpg"),
        R_letter(name:"wallet",Chinesename:"皮包",Phonetic:"ˋwɑlɪt",image:"wallet.jpg"),
        R_letter(name:"walnut",Chinesename:"胡桃",Phonetic:"ˋwɔlnət",image:"walnut.jpg"),
        R_letter(name:"walrus",Chinesename:"海象",Phonetic:"ˋwɔlrəs",image:"walrus.jpg"),
        R_letter(name:"wand",Chinesename:"棒",Phonetic:"wɑnd",image:"wand.jpg"),
        R_letter(name:"war",Chinesename:"戰爭",Phonetic:"wɔr",image:"war.jpg"),
        R_letter(name:"ward",Chinesename:"病房",Phonetic:"wɔrd",image:"ward.jpg"),
        R_letter(name:"warden",Chinesename:"典獄長",Phonetic:"ˋwɔrdn",image:"warden.jpg"),
        R_letter(name:"wardrobe",Chinesename:"衣櫥",Phonetic:"ˋwɔrd͵rob",image:"wardrobe.jpg"),
        R_letter(name:"warehouse",Chinesename:"倉庫",Phonetic:"ˋwɛr͵haʊs",image:"warehouse.jpg"),
        R_letter(name:"warrior",Chinesename:"武士",Phonetic:"ˋwɔrɪɚ",image:"warrior.jpg"),
        R_letter(name:"warship",Chinesename:"軍艦",Phonetic:"ˋwɔr͵ʃɪp",image:"warship.jpg"),
        R_letter(name:"washboard",Chinesename:"洗衣板",Phonetic:"ˋwɑʃ͵bord",image:"washboard.jpg"),
        R_letter(name:"washroom",Chinesename:"洗手間",Phonetic:"ˋwɑʃ͵rum",image:"washroom.jpg"),
        R_letter(name:"wasp",Chinesename:"黃蜂",Phonetic:"wɑsp",image:"wasp.jpg"),
        R_letter(name:"watch",Chinesename:"手錶",Phonetic:"wɑtʃ",image:"watch.jpg"),
        R_letter(name:"water",Chinesename:"水",Phonetic:"ˋwɔtɚ",image:"water.jpg"),
        R_letter(name:"waterfall",Chinesename:"瀑布",Phonetic:"ˋwɔtɚ͵fɔl",image:"waterfall.jpg"),
        R_letter(name:"waterman",Chinesename:"漁夫",Phonetic:"ˋwɔtɚmən",image:"waterman.jpg"),
        R_letter(name:"wave",Chinesename:"波",Phonetic:"wev",image:"wave.jpg"),
        R_letter(name:"wayfarer",Chinesename:"旅客",Phonetic:"ˋwe͵fɛrɚ",image:"wayfarer.jpg"),
        R_letter(name:"weapon",Chinesename:"武器",Phonetic:"ˋwɛpən",image:"weapon.jpg"),
        R_letter(name:"xerophyte",Chinesename:"旱生植物",Phonetic:"ˋzɪrə͵faɪt",image:"xerophyte.jpg"),
        R_letter(name:"Xerox",Chinesename:"複印",Phonetic:"ˋzɪrɑks",image:"Xerox.jpg"),
        R_letter(name:"X'mas",Chinesename:"聖誕節",Phonetic:"ˋkrɪsməs",image:"X'mas.jpg"),
        R_letter(name:"X-ray",Chinesename:"X光",Phonetic:"ˋɛksˋre",image:"X-ray.jpg"),
        R_letter(name:"xylem",Chinesename:"木質部",Phonetic:"ˋzaɪlɛm",image:"xylem.jpg"),
        R_letter(name:"xylophone",Chinesename:"木琴",Phonetic:"ˋzaɪlə͵fon",image:"xylophone.jpg"),
        R_letter(name:"yaChinesename",Chinesename:"快艇",Phonetic:"jɑt",image:"yaChinesename.jpg"),
        R_letter(name:"yak",Chinesename:"犛牛",Phonetic:"jæk",image:"yak.jpg"),
        R_letter(name:"yam",Chinesename:"山藥",Phonetic:"jæm",image:"yam.jpg"),
        R_letter(name:"yarn",Chinesename:"紗線",Phonetic:"jɑrn",image:"yarn.jpg"),
        R_letter(name:"yawl",Chinesename:"小艇",Phonetic:"jɔl",image:"yawl.jpg"),
        R_letter(name:"yawn",Chinesename:"呵欠",Phonetic:"jɔn",image:"yawn.jpg"),
        R_letter(name:"yeast",Chinesename:"泡沫",Phonetic:"jist",image:"yeast.jpg"),
        R_letter(name:"yellow",Chinesename:"黃色",Phonetic:"ˋjɛlo",image:"yellow.png"),
        R_letter(name:"yelp",Chinesename:"叫喊",Phonetic:"jɛlp",image:"yelp.jpg"),
        R_letter(name:"yeoman",Chinesename:"自耕農",Phonetic:"ˋjomən",image:"yeoman.jpg"),
        R_letter(name:"yew",Chinesename:"紫杉",Phonetic:"ju",image:"yew.jpg"),
        R_letter(name:"yoghurt",Chinesename:"優酪乳",Phonetic:"ˋjogɚt",image:"yoghurt.jpg"),
        R_letter(name:"yogurt",Chinesename:"優格",Phonetic:"ˋjogɚt",image:"yogurt.jpg"),
        R_letter(name:"yoke",Chinesename:"枷鎖",Phonetic:"jok",image:"yoke.jpg"),
        R_letter(name:"yokefellow",Chinesename:"同事",Phonetic:"ˋjok͵fɛlo",image:"yokefellow.jpg"),
        R_letter(name:"yokel",Chinesename:"鄉巴佬",Phonetic:"ˋjok!",image:"yokel.jpg"),
        R_letter(name:"yolk",Chinesename:"蛋黃",Phonetic:"jok",image:"yolk.png"),
        R_letter(name:"young",Chinesename:"年輕的",Phonetic:"jʌŋ",image:"young.jpg"),
        R_letter(name:"yo-yo",Chinesename:"溜溜球",Phonetic:"ˋjo͵jo",image:"yo-yo.jpg"),
        R_letter(name:"yule",Chinesename:"聖誕節",Phonetic:"jul",image:"yule.jpg"),
        R_letter(name:"zany",Chinesename:"小丑",Phonetic:"ˋzenɪ",image:"zany.png"),
        R_letter(name:"zealot",Chinesename:"狂熱分子",Phonetic:"ˋzɛlət",image:"zealot.jpg"),
        R_letter(name:"zebra",Chinesename:"斑馬",Phonetic:"ˋzibrə",image:"zebra.jpg"),
        R_letter(name:"zebu",Chinesename:"犛牛",Phonetic:"ˋzibju",image:"zebu.jpg"),
        R_letter(name:"zenith",Chinesename:"天頂",Phonetic:"ˋzinɪθ",image:"zenith.jpg"),
        R_letter(name:"zephyr",Chinesename:"和風",Phonetic:"ˋzɛfɚ",image:"zephyr.jpg"),
        R_letter(name:"zero",Chinesename:"零",Phonetic:"ˋzɪro",image:"zero.jpg"),
        R_letter(name:"Zeus",Chinesename:"宙斯",Phonetic:"zjus",image:"Zeus.jpg"),
        R_letter(name:"zion",Chinesename:"天國",Phonetic:"ˋzaɪən",image:"zion.jpg"),
        R_letter(name:"zip",Chinesename:"拉鍊",Phonetic:"zɪp",image:"zip.png"),
        R_letter(name:"zipcode",Chinesename:"郵政編號",Phonetic:"zɪp kod",image:"zipcode.jpg"),
        R_letter(name:"zipper",Chinesename:"拉鍊",Phonetic:"ˋzɪpɚ",image:"zipper.jpg"),
        R_letter(name:"zither",Chinesename:"箏",Phonetic:"ˋzɪθɚ",image:"zither.jpg"),
        R_letter(name:"zodiac",Chinesename:"十二宮圖",Phonetic:"ˋzodɪ͵æk",image:"zodiac.jpg"),
        R_letter(name:"zone",Chinesename:"地區",Phonetic:"zon",image:"zone.jpg"),
        R_letter(name:"zoo",Chinesename:"動物園",Phonetic:"zu",image:"zoo.jpg"),
        R_letter(name:"zygote",Chinesename:"受精卵",Phonetic:"ˋzaɪgot",image:"zygote.jpgvalentine")
        

    ]

    
 
 /*   var letters = [
        ["name":"rabbit","Chinesename":"兔子","Phonetic":"ˈrabət","image":"rabbit.jpg"],
        ["name":"racket","Chinesename":"球拍","Phonetic":"ˈrakət","image":"racket.jpg"],
        ["name":"raccoon","Chinesename":"浣熊","Phonetic":"raˈko͞on","image":"raccoon.jpg"],
        ["name":"ram","Chinesename":"公羊","Phonetic":"ram","image":"ram.jpg"],
        ["name":"radish","Chinesename":"小蘿蔔","Phonetic":"ˈradiSH","image":"radish.jpg"],
        ["name":"railway","Chinesename":"鐵路","Phonetic":"ˈrālˌwā","image":"railway.jpg"],
        ["name":"rainbow","Chinesename":"彩虹","Phonetic":"ˈrānˌbō","image":"rainbow.jpg"],
        ["name":"raisin","Chinesename":"葡萄乾","Phonetic":"ˈrāzən","image":"raisin.jpg"],
        ["name":"reservoir","Chinesename":"水庫","Phonetic":"ˈrezərˌvwär","image":"reservoir.jpg"],
        ["name":"restaurant","Chinesename":"餐廳","Phonetic":"ˈrest(ə)rənt","image":"restaurant.jpg"],
        ["name":"rice","Chinesename":"米飯","Phonetic":"rīs","image":"rice.jpg"],
        ["name":"rick","Chinesename":"乾草堆","Phonetic":"rik","image":"rick.jpg"],
        ["name":"rink","Chinesename":"溜冰場","Phonetic":"riNGk","image":"rink.jpg"],
        ["name":"river","Chinesename":"河","Phonetic":"ˈrivər","image":"river.jpg"],
        ["name":"robot","Chinesename":"機器人","Phonetic":"ˈrōbət","image":"robot.jpg"],
        ["name":"refrigerator","Chinesename":"冰箱","Phonetic":"rəˈfrijəˌrādər","image":"refrigerator.jpg"],
        ["name":"rose","Chinesename":"玫瑰花","Phonetic":"rōz","image":"rose.jpg"],
        ["name":"rosemary","Chinesename":"迷迭香","Phonetic":"ˈrōzˌmerē","image":"rosemary.jpg"],
        ["name":"rhinoceros","Chinesename":"犀牛","Phonetic":"rīˈnäs(ə)rəs","image":"rhinoceros.jpg"],
        ["name":"redbreast","Chinesename":"知更鳥","Phonetic":"ˈredˌbrest","image":"redbreast.jpg"],
        ["name":"redwing","Chinesename":"紅翼鶇","Phonetic":"ˈredwiNG","image":"redwing.jpg"],
        ["name":"reindeer","Chinesename":"巡鹿","Phonetic":"ˈrānˌdi","image":"reindeer.jpg"],
        ["name":"rooster","Chinesename":"公雞","Phonetic":"ˈro͞ostər","image":"rooster.jpg"],
        ["name":"razorback","Chinesename":"野豬","Phonetic":"ˈrāzərˌbak","image":"razorback.jpg"],
        ["name":"redbird","Chinesename":"紅雀","Phonetic":"ˈredbə:d","image":"redbird.jpg"],
        ["name":"redhead","Chinesename":"紅頭鴨","Phonetic":"ˈredˌhed","image":"redhead.jpg"],
        ["name":"red squirrel","Chinesename":"紅松鼠","Phonetic":"red ˈskwər(ə)l","image":"red squirrel.jpg"],
        ["name":"ringdove","Chinesename":"斑鳩","Phonetic":"ˈrōˌbək","image":"ringdove.jpg"],
        ["name":"roebuck","Chinesename":"雄鹿","Phonetic":"ˈrōˌbək","image":"roebuck.jpg"],
        ["name":"rainbow trout","Chinesename":"虹鱒魚","Phonetic":"ˈrānbō ˈtrout","image":"rainbow trout.jpg"],
    ]
*/
    override func viewDidLoad() {
        super.viewDidLoad()
        saveEnglish(letters : letters)
        getword()
        searchController = UISearchController(searchResultsController : nil)
        tableView.tableHeaderView = searchController?.searchBar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return letters.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "r_letterCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as!
            r_letterTableViewCell

        // Configure the cell...

        cell.EnglishLabel.text = letters [indexPath.row].name
        cell.ChineseLabel.text = letters [indexPath.row].Chinesename
        cell.PhoneticLabel.text = letters [indexPath.row].Phonetic
        cell.thumbnailImageView.image = UIImage(named: letters [indexPath.row].image)

        
        return cell
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showr_letterDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination
                    as! r_letterViewController
                destinationController.letters = letters[indexPath.row]
            }
        }
    }
    
/*    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let optionMenu = UIAlertController(title: nil, message: "You can do...", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        optionMenu.addAction(cancelAction)
        present(optionMenu,animated: true, completion: nil)
        
        let callActionHandler = {(action:UIAlertAction!) ->Void in
            let synth = AVSpeechSynthesizer()
            let myUtterance = AVSpeechUtterance(string:self.letters[indexPath.row]["name"]!)
            myUtterance.rate = 0.4
            myUtterance.pitchMultiplier = 1.5
            myUtterance.volume = 5
            myUtterance.voice = AVSpeechSynthesisVoice(language: "en_US")
            synth.speak(myUtterance)
        
        
        }
        let callAction = UIAlertAction(title: "speak", style: .default, handler: callActionHandler)
    
        optionMenu.addAction(callAction)
    
    
    

    }
*/

    override func tableView(_ tableView: UITableView, commit editingStyel: UITableViewCellEditingStyle,forRowAt indexPath: IndexPath){
        
        if editingStyel == .delete{
            letters.remove(at: indexPath.row)
            
        }
        tableView.deleteRows(at: [indexPath], with: .fade)
        
        
        
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) ->[UITableViewRowAction]?{
        
        let shareAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "SHARE", handler:{ (action, indexPath) ->Void in
        
               let defaultText = "Just hecking in at " + self.letters[indexPath.row].name
               let activityController = UIActivityViewController(activityItems: [defaultText], applicationActivities: nil)
               self.present(activityController, animated: true,completion: nil)
        })
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "DELETE"){ (action, indexPath) ->Void in
        
            self.letters.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
    
            let chinesevoice = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "speak"){ (action,indexPath) -> Void in
                let string = self.letters [indexPath.row].Chinesename
                let synthesizer = AVSpeechSynthesizer()
                let voice = AVSpeechSynthesisVoice(language:"en_US")
                let utterance = AVSpeechUtterance(string: string)
                utterance.voice = voice
                synthesizer.speak(utterance)
            }
            return [deleteAction, shareAction, chinesevoice]
            
    }
           
            
   //搜尋列
    
  

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
