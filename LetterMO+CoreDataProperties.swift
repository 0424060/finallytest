//
//  LetterMO+CoreDataProperties.swift
//  0424038_HW0414
//
//  Created by ChenDing on 2017/6/22.
//  Copyright © 2017年 nkfust. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension LetterMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LetterMO> {
        return NSFetchRequest<LetterMO>(entityName: "Letter");
    }

    @NSManaged public var name: String?
    @NSManaged public var image: NSData?
    @NSManaged public var chinese: String?
    @NSManaged public var english: String?

}
