//
//  r_letterTableViewCell.swift
//  0424038＿HW0414
//
//  Created by ChenDing on 2017/4/15.
//  Copyright © 2017年 nkfust. All rights reserved.
//

import UIKit

class r_letterTableViewCell: UITableViewCell {
    
    
    @IBOutlet var EnglishLabel:UILabel!
    @IBOutlet var ChineseLabel:UILabel!
    @IBOutlet var PhoneticLabel:UILabel!
    @IBOutlet var IDLabel:UILabel!
    @IBOutlet var thumbnailImageView:UIImageView!
    
    var rletters: R_letter!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
